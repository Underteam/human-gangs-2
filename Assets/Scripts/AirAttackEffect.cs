﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirAttackEffect : MonoBehaviour
{

    public GameObject AirEffect;
    GameObject airEffect;
    public GameObject AirEffectSnd;
    GameObject airEffectSnd;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Ground"))
        {
            airEffect = Instantiate(AirEffect, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.7f, gameObject.transform.position.z), Quaternion.Euler(-90,0,0));
            Destroy(airEffect, 2f);
            airEffectSnd = Instantiate(AirEffectSnd, transform.position, transform.rotation);
            Destroy(airEffectSnd, 2f);
            gameObject.GetComponent<SphereCollider>().enabled = false;
        }
    }
}
