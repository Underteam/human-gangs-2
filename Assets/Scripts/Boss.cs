﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{

    public GameObject Cannon;
    public GameObject[] Bullet;
    public GameObject Aim;
    int BulletIndex;
    public bool isFire = false;
    GameObject bullet;
    private float shotCounter;
    public float fireRate;
    public float fastFireRate;
    public bool fastFire;
    public float force1;
    public float force2;
    public float angle1;
    public float angle2;
    public int HP;
    public int maxHp;
    public GameObject leftHand;
    public GameObject rightHand;
    public GameObject Grabbed;
    protected Animator _animatorBoss;
    Animator animatorBoss;
    public GameObject ShotEffect;
    GameObject shotEffect;
    float HpIndex;
    public GameObject HpBar;
    public int BulletNumber;

    // Start is called before the first frame update
    void Start()
    {
        animatorBoss = GetComponent<Animator>();
        HpIndex = 1f / HP;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFire)
        {
            if (shotCounter <= 0)
            {
                if (fastFire)
                {
                    shotCounter = fastFireRate;
                }
                else
                {
                    shotCounter = fireRate;
                }
                Shoot();
            }
            else
                shotCounter -= Time.deltaTime;
        }
        HpBar.GetComponent<Scrollbar>().size = HP * HpIndex;
        if (HP <= 0)
        {
            HpBar.SetActive(false);
        }
        if (HP >= maxHp)
        {
            HP = maxHp;
        }
    }
    public void Shoot()
    {
        BulletIndex++;
        if (BulletIndex >= BulletNumber)
        {
            BulletIndex = 0;
        }
        Aim.transform.localRotation = Quaternion.Euler(UnityEngine.Random.Range(angle1, angle2), 0, 0);
        
        if (Bullet[BulletIndex].transform.parent == Grabbed.transform)
        {
            //Bullet[BulletIndex].transform.parent = null;
            Bullet[BulletIndex].GetComponent<MeshCollider>().isTrigger = false;
            Bullet[BulletIndex].GetComponent<PickedItem>().picked = 0;
            Bullet[BulletIndex].transform.position = Aim.transform.position;
            Bullet[BulletIndex].transform.rotation = Aim.transform.rotation;
            Bullet[BulletIndex].GetComponent<Rigidbody>().isKinematic = true;
            Bullet[BulletIndex].transform.gameObject.tag = "BulletForPlayer";
            shotEffect = Instantiate(ShotEffect, Aim.transform.position, Aim.transform.rotation);
            StartCoroutine(Kinematic());
        }
    }

    public IEnumerator Kinematic()
    {
        yield return new WaitForSeconds(0.01f);
        Bullet[BulletIndex].GetComponent<Rigidbody>().isKinematic = false;
        Bullet[BulletIndex].GetComponent<Rigidbody>().AddForce(Aim.transform.forward * UnityEngine.Random.Range(force1, force2), ForceMode.Impulse);
    }

    //private void OnTriggerEnter(Collider col)
    //{
    //    if (col.CompareTag("Throwing"))
    //    {
    //        HP -= 1;
    //        GetComponent<Animator>().speed += 0.2f;
    //        fireRate *= 0.8f;
    //        fastFireRate *= 0.8f;
    //    }
    //}
}
