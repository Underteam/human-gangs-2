﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss1CutScene : MonoBehaviour
{

    public GameObject CutScene;
    public GameObject Gate1;
    public GameObject Gate2;
    public GameObject Door1;
    public GameObject Door2;
    public GameObject Boss;
    public GameObject PlayerCam;
    public bool End;
    public GameObject PlayerFallCol;
    public GameObject Hp;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (End)
        {
            Gate1.SetActive(true);
            Gate2.SetActive(true);
            CutScene.SetActive(false);
            PlayerCam.SetActive(true);
            Boss.SetActive(true);
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = true;
            Hp.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "ColPlayer")
        {
            Gate1.SetActive(false);
            Gate2.SetActive(false);
            CutScene.SetActive(true);
            PlayerCam.SetActive(false);
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = false;
            Hp.SetActive(false);
            Door1.transform.localRotation = Quaternion.Euler(-180, 0, 0);
            Door2.transform.localRotation = Quaternion.Euler(0, 0, 0);
            Destroy(gameObject);
        }
    }
}
