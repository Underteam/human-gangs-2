﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCol : MonoBehaviour
{

    public GameObject boss;
    public GameObject Gear;
    public GameObject Gate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Throwing"))
        {
            boss.GetComponent<Boss>().HP -= 1;
            boss.GetComponent<Animator>().speed += 0.2f;
            boss.GetComponent<Boss>().fireRate *= 0.8f;
            boss.GetComponent<Boss>().fastFireRate *= 0.8f;
            if (boss.GetComponent<Boss>().HP > 0)
            {
                Gear.GetComponent<Animator>().enabled = true;
            }
            else
            {
                GetComponent<Animator>().enabled = true;
                boss.GetComponent<Animator>().SetBool("Defeat", true);
                boss.GetComponent<Animator>().enabled = false;
                Gate.GetComponent<Animator>().enabled = true;
                StartCoroutine(BossHide());
            }
            StartCoroutine(AnimationOff());
        }
    }

    public IEnumerator AnimationOff()
    {
        yield return new WaitForSeconds(1f);
        Gear.GetComponent<Animator>().enabled = false;
    }
    public IEnumerator BossHide()
    {
        yield return new WaitForSeconds(1f);
        boss.SetActive(false);
    }
}
