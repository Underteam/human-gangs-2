﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakBridge : MonoBehaviour
{

    public GameObject BrokenBridge;
    GameObject brokenBridge;
    public GameObject rotator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "ColPlayer")
        {
            Instantiate(BrokenBridge, gameObject.transform.position, rotator.transform.rotation);
            Destroy(gameObject);
        }
    }
}
