﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeTrigger : MonoBehaviour
{

    public GameObject Bridge;
    public GameObject Hints;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Throwing"))
        {
            Bridge.GetComponent<Animator>().enabled = true;
            Hints.SetActive(false);
        }
    }
}
