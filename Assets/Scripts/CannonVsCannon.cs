﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonVsCannon : MonoBehaviour
{

    public int Rotated;
    public GameObject Enemy;
    public GameObject Boss1;
    public GameObject Boss2;
    public GameObject AnimatedUnicorn;
    public GameObject Unicorn;
    public GameObject Cut2;
    public GameObject PlayerFallCol;
    public GameObject Hp;
    public bool End;
    public bool LusterDown;
    public GameObject Luster;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (End)
        {
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = true;
            Hp.SetActive(true);
            Unicorn.SetActive(true);
            Destroy(AnimatedUnicorn);
            Cut2.SetActive(false);
        }
        if (LusterDown)
        {
            Luster.GetComponent<Animator>().enabled = true;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "CannonHandle")
        {
            Rotated += 1;
            Enemy.GetComponent<CannonVsCannon>().Rotated += 1;
            if(Rotated >= 2)
            {
                Cut2.SetActive(true);
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = false;
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = false;
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = false;
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = false;
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = false;
                PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = false;
                Hp.SetActive(false);
                Destroy(Boss1);
                Destroy(Boss2);
            }
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name == "CannonHandle")
        {
            Rotated -= 1;
            Enemy.GetComponent<CannonVsCannon>().Rotated -= 1;
        }
    }
}
