﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashGrabbed : MonoBehaviour {

	public GameObject crashedObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter (Collider col){
		if (col.CompareTag ("forCrashes") | col.CompareTag ("PlayerFists")) {
			Instantiate (crashedObject, transform.position, transform.rotation);
			Destroy (gameObject);
		}
	}
}
