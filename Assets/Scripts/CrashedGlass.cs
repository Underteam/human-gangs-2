﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashedGlass : MonoBehaviour {

	public GameObject fixSize;

	// Use this for initialization
	void Start () {
		gameObject.transform.parent = GameObject.Find("FixSize(Clone)").transform;
		StartCoroutine (fixDelay ());
		Destroy (gameObject, UnityEngine.Random.Range (6f, 8.2f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public IEnumerator fixDelay(){
		yield return new WaitForSeconds (8f);
		Destroy (GameObject.Find("FixSize(Clone)"));
		Destroy (GameObject.Find("crashed glass(Clone)"));
	}
}
