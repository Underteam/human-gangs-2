﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneJump : MonoBehaviour
{

    public GameObject Player;
    public GameObject PlayerCoords;
    public GameObject PlayerFixCoords;
    public GameObject Camera;
    public GameObject Cutscene;
    public GameObject PlayerFallCol;
    public GameObject Hp;
    public bool End;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (End)
        {
            //Player.SetActive(true);
            Camera.SetActive(true);
            PlayerFixCoords.transform.position = PlayerCoords.transform.position;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = true;
            Hp.SetActive(true);

            Cutscene.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == ("ColPlayer"))
        {
            PlayerFixCoords.transform.position = PlayerCoords.transform.position;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = false;
            Hp.SetActive(false);
            Camera.SetActive(false);
            Cutscene.SetActive(true);
            Destroy(gameObject);
        }
    }
}
