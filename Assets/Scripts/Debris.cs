﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Destroy(gameObject, UnityEngine.Random.Range(4f, 6f));
        GetComponent <Rigidbody> ().AddForce (GameObject.Find("ColPlayer").transform.forward * 10, ForceMode.Impulse);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public IEnumerator destroyDelay(){
		yield return new WaitForSeconds (5f);
		Destroy (gameObject, UnityEngine.Random.Range (4f, 6f));
	}
}
