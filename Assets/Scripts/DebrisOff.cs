﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisOff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, UnityEngine.Random.Range(4f, 6f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
