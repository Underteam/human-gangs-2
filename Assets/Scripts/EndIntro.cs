﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndIntro : MonoBehaviour
{

    public GameObject Menu;
    public GameObject Loading;

    // Start is called before the first frame update
    void Start()
    {
        Menu.GetComponent<Menu>().ShowAd();
        Loading.SetActive(true);
        StartCoroutine(EndDelay());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(0.1f);
        Application.LoadLevel(2);
    }
}
