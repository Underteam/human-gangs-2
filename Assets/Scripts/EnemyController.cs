﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public GameObject Head;
	public GameObject FixCoords;
	private float walkCounter;
	public GameObject rightLeg;
	public GameObject leftLeg;
	public GameObject Hips;
    Animator animatorPlayer;
    public GameObject rightHand;
    public GameObject leftHand;

    // Use this for initialization
    void Start () {
        animatorPlayer = FixCoords.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //		Hips.transform.rotation = FixCoords.transform.rotation;
        Head.transform.rotation = FixCoords.transform.rotation;
        //		if (walkCounter <= 0) {
        //			walkCounter = 0.6f;
        //			rightLeg.GetComponent <Rigidbody> ().AddForce (transform.forward * 8, ForceMode.Impulse);
        //			StartCoroutine (rightLegBack ());
        //			StartCoroutine (leftLegDelay ());
        //		} else
        //			walkCounter -= Time.deltaTime;

        if (animatorPlayer.GetFloat("Forward") > 0.1f)
        {
            if (walkCounter <= 0)
            {
                walkCounter = 0.6f;
                rightLeg.GetComponent<Rigidbody>().AddForce(FixCoords.transform.forward * 8, ForceMode.Impulse);
                leftLeg.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.forward * 8, ForceMode.Impulse);
                StartCoroutine(rightLegBack());
                StartCoroutine(leftLegDelay());
                leftHand.GetComponent<Rigidbody>().AddForce(FixCoords.transform.forward * 4, ForceMode.Impulse);
                leftHand.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.right * 2, ForceMode.Impulse);
                rightHand.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.forward * 4, ForceMode.Impulse);
                rightHand.GetComponent<Rigidbody>().AddForce(FixCoords.transform.right * 2, ForceMode.Impulse);
                StartCoroutine(rightHandDelay());
                StartCoroutine(leftHandBack());
            }
            else
                walkCounter -= Time.deltaTime;
        }
    }
    public IEnumerator leftLegDelay()
    {
        yield return new WaitForSeconds(0.3f);
        leftLeg.GetComponent<Rigidbody>().AddForce(FixCoords.transform.forward * 8, ForceMode.Impulse);
    }
    public IEnumerator rightLegBack()
    {
        yield return new WaitForSeconds(0.3f);
        rightLeg.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.forward * 8, ForceMode.Impulse);
    }

    public IEnumerator rightHandDelay()
    {
        yield return new WaitForSeconds(0.3f);
        rightHand.GetComponent<Rigidbody>().AddForce(FixCoords.transform.forward * 4, ForceMode.Impulse);
        rightHand.GetComponent<Rigidbody>().AddForce(FixCoords.transform.right * 2, ForceMode.Impulse);
    }
    public IEnumerator leftHandBack()
    {
        yield return new WaitForSeconds(0.3f);
        leftHand.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.forward * 4, ForceMode.Impulse);
        leftHand.GetComponent<Rigidbody>().AddForce(-1 * FixCoords.transform.right * 2, ForceMode.Impulse);
    }
}
