﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Cameras;

public class EnemyFall : MonoBehaviour {

	public GameObject hips;
	public GameObject Head;
	public GameObject fixCoords;
	public GameObject Col;
	public GameObject colforStop;
	GameObject Player;
	public GameObject enemyController;
	public GameObject Enemy;
	public GameObject Grabbed;
	public GameObject playerThrower;
	public GameObject[] throwCols;
	public GameObject hitEffect;
	GameObject hiteffect;
	public int Hp;
    public int maxHp;
    public GameObject fist1;
	public GameObject fist2;
	public GameObject MenuSc;
    int FallIndex;
    public GameObject PlayerCamera;
    GameObject playerCamera;
    GameObject pivotCam;
    GameObject Cam1;
    GameObject Cam2;
    int SlowIndex;
    public bool Boss = false;
    float HpIndex;
    public GameObject HpBar;
    public GameObject Cut3;
    public GameObject playerCharacter;
    public GameObject Luster;
    GameObject Cap;
    GameObject WindowPart;
    public GameObject PlayerFallCol;
    public GameObject PlayerHp;

    // Use this for initialization
    void Start () {
        playerCamera = GameObject.Find("Main Camera");
        HpIndex = 1f / Hp;
    }
	
	// Update is called once per frame
	void Update () {
        //		Head.transform.localRotation = Quaternion.Euler (0, 0, 0);
        if (Boss)
        {
            HpBar.GetComponent<Scrollbar>().size = Hp * HpIndex;
            if (Hp <= 0)
            {
                HpBar.SetActive(false);
            }
            if (Hp >= maxHp)
            {
                Hp = maxHp;
            }
        }
    }

	private void OnTriggerEnter (Collider col){
		if (col.CompareTag ("PlayerFists")) {
            if (!Boss)
            {
                Hp--;
                hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
                Destroy(hiteffect, 2);
                FallIndex = Random.Range(0, 10);
                SlowIndex = Random.Range(0, 2);
                if (FallIndex == 9)
                {
                    FallNoSlow();
                    //StartCoroutine(RaiseDelay());
                }
                if (Hp <= 0)
                {
                    if (SlowIndex > 0)
                    {
                        Fall();
                    }
                    else
                    {
                        FallNoSlow();
                    }
                }
            }
        }
		//if (col.CompareTag ("Grabs")) {
		//	Enemy.SetActive (false);
		//	Grabbed.SetActive (true);
		//}
		if (col.CompareTag ("Throwing")) {
            if (!Boss)
            {
                Hp -= 10;
                hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
                Destroy(hiteffect, 2);
                if (Hp <= 0)
                {
                    playerCamera = GameObject.Find("FreeLookCameraRig");
                    playerCamera.GetComponent<FreeLookCam>().enabled = false;
                    playerCamera.GetComponent<ProtectCameraFromWallClip>().enabled = false;
                    playerCamera.transform.position = gameObject.transform.position;
                    playerCamera.transform.rotation = Quaternion.Euler(Random.Range(-20, 20), Random.Range(0, 360), Random.Range(-25, 25));
                    Fall();
                }
                else
                {
                    FallNoSlow();
                }
            }
		}
        if (col.gameObject.name == "AirAttackCol")
        {
            if (!Boss)
            {
                Hp -= 3;
                hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
                Destroy(hiteffect, 2);
                playerCamera = GameObject.Find("FreeLookCameraRig");
                pivotCam = GameObject.Find("Pivot");
                Cam1 = GameObject.Find("cam1");
                Cam2 = GameObject.Find("cam2");
                pivotCam.transform.position = Cam2.transform.position;
                //playerCamera.transform.rotation = Quaternion.Euler(Random.Range(-20, 20), Random.Range(0, 360), Random.Range(-25, 25));
                AirFall();
            }
        }
        if (col.CompareTag("Luster"))
        {
            if (Boss)
            {
                Hp -= 1;
                hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
                Destroy(hiteffect, 2);
                playerCamera = GameObject.Find("FreeLookCameraRig");
                pivotCam = GameObject.Find("Pivot");
                Cam1 = GameObject.Find("cam1");
                Cam2 = GameObject.Find("cam2");
                pivotCam.transform.position = Cam2.transform.position;
                //playerCamera.transform.rotation = Quaternion.Euler(Random.Range(-20, 20), Random.Range(0, 360), Random.Range(-25, 25));
                AirFall();
            }
        }
        if (col.gameObject.name == "ColliderBottom")
        {
            Hp -= 10;
            hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
            Destroy(hiteffect, 2);
            CarFall();
        }
        if (col.gameObject.name == "FallTrigger")
        {
            Hp = 0;
            fist1.SetActive(false);
            fist2.SetActive(false);
            hips.GetComponent<PlayerFixCoords>().enabled = false;
            Head.GetComponent<ConstantForce>().enabled = false;
            Col.GetComponent<CapsuleCollider>().enabled = false;
            Col.GetComponent<SphereCollider>().enabled = false;
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
            enemyController.GetComponent<EnemyController>().enabled = false;
            Head.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            hips.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
            colforStop.SetActive(false);
            Destroy(Enemy, 3);
            MenuSc.GetComponent<Menu>().Score += 1;
        }
    }
	private void Fall(){
		hips.GetComponent <PlayerFixCoords> ().enabled = false;
		Head.GetComponent <ConstantForce> ().enabled = false;
		Col.GetComponent <CapsuleCollider> ().enabled = false;
		Col.GetComponent <SphereCollider> ().enabled = false;
		gameObject.GetComponent <CapsuleCollider> ().enabled = false;
		enemyController.GetComponent <EnemyController> ().enabled = false;
		Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
		hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
        Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
        hips.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * 100, ForceMode.Impulse);
		hips.GetComponent <Rigidbody> ().AddForce (Player.transform.up * 120, ForceMode.Impulse);
		colforStop.SetActive (false);
		StartCoroutine (SlowMo ());
		StartCoroutine (TimeDelay ());
		if (Hp > 0)
			StartCoroutine (RaiseDelay ());
		else {
			fist1.SetActive (false);
			fist2.SetActive (false);
            Destroy(Enemy, 3);
            GetComponent<AudioSource>().enabled = true;
            //MenuSc.GetComponent <Menu> ().Score += 1;
		}
    }

    private void AirFall()
    {
        hips.GetComponent<PlayerFixCoords>().enabled = false;
        Head.GetComponent<ConstantForce>().enabled = false;
        Col.GetComponent<CapsuleCollider>().enabled = false;
        Col.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        enemyController.GetComponent<EnemyController>().enabled = false;
        Head.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        hips.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        hips.GetComponent<Rigidbody>().AddForce(-fixCoords.transform.forward * 50, ForceMode.Impulse);
        Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
        hips.GetComponent<Rigidbody>().AddForce(Player.transform.up * 130, ForceMode.Impulse);
        colforStop.SetActive(false);
        StartCoroutine(SlowMo());
        StartCoroutine(TimeDelay());
        if (Hp > 0)
            StartCoroutine(RaiseDelay());
        else
        {
            fist1.SetActive(false);
            fist2.SetActive(false);
            if (!Boss)
            {
                Destroy(Enemy, 3);
                GetComponent<AudioSource>().enabled = true;
                //MenuSc.GetComponent<Menu>().Score += 1;
            }
            else
            {
                if (Hp <= 0)
                {
                    StartCoroutine(FinalBossDefeat());
                }
            }
        }
    }
    private void CarFall()
    {
        hips.GetComponent<PlayerFixCoords>().enabled = false;
        Head.GetComponent<ConstantForce>().enabled = false;
        Col.GetComponent<CapsuleCollider>().enabled = false;
        Col.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        enemyController.GetComponent<EnemyController>().enabled = false;
        Head.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        hips.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //hips.GetComponent<Rigidbody>().AddForce(-fixCoords.transform.forward * 50, ForceMode.Impulse);
        hips.GetComponent<Rigidbody>().AddForce(transform.up * 110, ForceMode.Impulse);
        hips.GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderForStop").transform.forward * 200, ForceMode.Impulse);
        colforStop.SetActive(false);
        if (Hp > 0)
            StartCoroutine(RaiseDelay());
        else
        {
            fist1.SetActive(false);
            fist2.SetActive(false);
            Destroy(Enemy, 3);
            GetComponent<AudioSource>().enabled = true;
            //MenuSc.GetComponent<Menu>().Score += 1;
        }
        GetComponent<AudioSource>().enabled = true;
    }

    private void FallNoSlow()
    {
        hips.GetComponent<PlayerFixCoords>().enabled = false;
        Head.GetComponent<ConstantForce>().enabled = false;
        Col.GetComponent<CapsuleCollider>().enabled = false;
        Col.GetComponent<SphereCollider>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        enemyController.GetComponent<EnemyController>().enabled = false;
        Head.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        hips.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
        hips.GetComponent<Rigidbody>().AddForce(Player.transform.forward * 100, ForceMode.Impulse);
        hips.GetComponent<Rigidbody>().AddForce(Player.transform.up * 120, ForceMode.Impulse);
        colforStop.SetActive(false);
        if (Hp > 0)
            StartCoroutine(RaiseDelay());
        else
        {
            fist1.SetActive(false);
            fist2.SetActive(false);
            Destroy(Enemy, 3);
            GetComponent<AudioSource>().enabled = true;
            //MenuSc.GetComponent<Menu>().Score += 1;
        }
    }
    public IEnumerator RaiseDelay(){
		yield return new WaitForSeconds (Random.Range(1.8f, 4f));
		fixCoords.transform.position = new Vector3 (hips.transform.position.x, hips.transform.position.y, hips.transform.position.z);
		fixCoords.SetActive (true);
		Head.GetComponent <ConstantForce> ().enabled = true;
		Col.GetComponent <CapsuleCollider> ().enabled = true;
		Col.GetComponent <SphereCollider> ().enabled = true;
		gameObject.GetComponent <CapsuleCollider> ().enabled = true;
		hips.GetComponent <PlayerFixCoords> ().enabled = true;
		hips.transform.rotation = fixCoords.transform.rotation;
		Head.transform.rotation = hips.transform.rotation;
		colforStop.SetActive (true);
//		Head.transform.rotation = Quaternion.identity;
		Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.FreezeRotation;
		hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.FreezePosition;
//		yield return new WaitForSeconds (0.2f);
		enemyController.GetComponent <EnemyController> ().enabled = true;
		throwCols[0].transform.gameObject.tag = "Untagged";
		throwCols[1].transform.gameObject.tag = "Untagged";
		throwCols[2].transform.gameObject.tag = "Untagged";
		throwCols[3].transform.gameObject.tag = "Untagged";
		throwCols[4].transform.gameObject.tag = "Untagged";
		throwCols[5].transform.gameObject.tag = "Untagged";
		throwCols[6].transform.gameObject.tag = "Untagged";
		throwCols[7].transform.gameObject.tag = "Untagged";
		throwCols[8].transform.gameObject.tag = "Untagged";
		throwCols[9].transform.gameObject.tag = "Untagged";
		throwCols[10].transform.gameObject.tag = "Untagged";
	}
	public IEnumerator SlowMo(){
		yield return new WaitForSeconds (0.3f);
        playerCamera = GameObject.Find("Main Camera");
        //playerCamera.GetComponent<BloomOptimized>().enabled = true;
        //playerCamera.GetComponent<VignetteAndChromaticAberration>().enabled = true;
        playerCamera.GetComponent<AudioSource>().enabled = true;
        pivotCam = GameObject.Find("Pivot");
        pivotCam.GetComponent<AudioSource>().enabled = false;
        Time.timeScale = 0.2f;
        //Time.fixedDeltaTime = Time.timeScale * 0.05f;
        //StartCoroutine(FixTimeDelay());
    }

    public IEnumerator FinalBossDefeat()
    {
        yield return new WaitForSeconds(2f);
        Cut3.SetActive(true);
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = false;
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = false;
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = false;
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = false;
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = false;
        PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = false;
        PlayerHp.SetActive(false);
        playerCharacter.SetActive(false);
        Luster.SetActive(false);
        Cap = GameObject.Find("Cap (1)");
        WindowPart = GameObject.Find("WindowPart (1)");
        Destroy(Cap);
        Destroy(WindowPart);
        Destroy(Enemy);
    }

    public IEnumerator TimeDelay(){
		yield return new WaitForSeconds (0.5f);
        playerCamera = GameObject.Find("Main Camera");
        //playerCamera.GetComponent<BloomOptimized>().enabled = false;
        //playerCamera.GetComponent<VignetteAndChromaticAberration>().enabled = false;
        playerCamera.GetComponent<AudioSource>().enabled = false;
        Time.timeScale = 1;
        //Time.fixedDeltaTime = Time.unscaledDeltaTime;
        playerCamera = GameObject.Find("FreeLookCameraRig");
        playerCamera.GetComponent<FreeLookCam>().enabled = true;
        playerCamera.GetComponent<ProtectCameraFromWallClip>().enabled = true;
        pivotCam = GameObject.Find("Pivot");
        Cam1 = GameObject.Find("cam1");
        Cam2 = GameObject.Find("cam2");
        pivotCam.transform.position = Cam1.transform.position;
        pivotCam.GetComponent<AudioSource>().enabled = true;
    }
    public IEnumerator FixTimeDelay()
    {
        yield return new WaitForSeconds(0.3f);
        Time.fixedDeltaTime = Time.unscaledDeltaTime;
    }

    public void Throw (){
		hips.GetComponent <PlayerFixCoords> ().enabled = false;
		Head.GetComponent <ConstantForce> ().enabled = false;
		Col.GetComponent <CapsuleCollider> ().enabled = false;
//		Col.GetComponent <SphereCollider> ().enabled = false;
		gameObject.GetComponent <CapsuleCollider> ().enabled = false;
		Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
		hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
		StartCoroutine (RaiseDelay ());
		hips.transform.position = playerThrower.transform.position;
		hips.transform.rotation = playerThrower.transform.rotation;
        Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
        hips.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * 80, ForceMode.Impulse);
		hips.GetComponent <Rigidbody> ().AddForce (Player.transform.up * 50, ForceMode.Impulse);
		Grabbed.SetActive (false);
		throwCols[0].transform.gameObject.tag = "PlayerFists";
		throwCols[1].transform.gameObject.tag = "PlayerFists";
		throwCols[2].transform.gameObject.tag = "PlayerFists";
		throwCols[3].transform.gameObject.tag = "PlayerFists";
		throwCols[4].transform.gameObject.tag = "PlayerFists";
		throwCols[5].transform.gameObject.tag = "PlayerFists";
		throwCols[6].transform.gameObject.tag = "PlayerFists";
		throwCols[7].transform.gameObject.tag = "PlayerFists";
		throwCols[8].transform.gameObject.tag = "PlayerFists";
		throwCols[9].transform.gameObject.tag = "PlayerFists";
		throwCols[10].transform.gameObject.tag = "PlayerFists";
	}
}
