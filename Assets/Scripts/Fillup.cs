﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class Fillup : MonoBehaviour
{

    public GameObject Canister;
    public GameObject CarEnter;
    public GameObject fillHint;
    public GameObject KeyHint;
    public GameObject Car;
    public GameObject Key1;
    public GameObject Key2;
    public GameObject KeyPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "canister")
        {
            CarEnter.SetActive(true);
            Car.GetComponent<CarAudio>().enabled = true;
            Destroy(Canister);
            Destroy(fillHint);
            KeyHint.SetActive(false);
        }
        if (col.gameObject.name == "key (1)")
        {
            if (Key1.GetComponent<PickedItem>().picked == 0)
            {
                Key1.GetComponent<Rigidbody>().isKinematic = true;
                StartCoroutine(Kinematic1Delay());
                Key1.GetComponent<MeshCollider>().isTrigger = false;
                Key1.GetComponent<PickedItem>().picked = 0;
                Key1.transform.parent = null;
                Key1.transform.position = KeyPosition.transform.position;
                Key1.transform.rotation = KeyPosition.transform.rotation;
                KeyHint.SetActive(false);
            }
        }
        if (col.gameObject.name == "key (2)")
        {
            if(Key2.GetComponent<PickedItem>().picked == 0)
            {
                Key2.GetComponent<Rigidbody>().isKinematic = true;
                StartCoroutine(Kinematic2Delay());
                Key2.GetComponent<MeshCollider>().isTrigger = false;
                Key2.GetComponent<PickedItem>().picked = 0;
                Key2.transform.parent = null;
                Key2.transform.position = KeyPosition.transform.position;
                Key2.transform.rotation = KeyPosition.transform.rotation;
                KeyHint.SetActive(false);
            }
        }
    }

    public IEnumerator Kinematic1Delay()
    {
        yield return new WaitForSeconds(0.1f);
        Key1.GetComponent<Rigidbody>().isKinematic = false;
    }
    public IEnumerator Kinematic2Delay()
    {
        yield return new WaitForSeconds(0.1f);
        Key2.GetComponent<Rigidbody>().isKinematic = false;
    }
}
