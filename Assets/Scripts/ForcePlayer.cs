﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcePlayer : MonoBehaviour {

	public float forcePower;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent <Rigidbody> ().AddForce (new Vector3 (0,90,0) * forcePower, ForceMode.Force);
	}
}
