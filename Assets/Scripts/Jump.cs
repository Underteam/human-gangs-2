﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

	public bool jumpInd;
	public GameObject PlayerFix;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void jump (){
		if (jumpInd)
			PlayerFix.GetComponent<Rigidbody> ().AddForce (new Vector3 (0,90,0) * 0.07f, ForceMode.Impulse);
	}

	private void OnTriggerStay (Collider col){
		jumpInd = true;
	}
	private void OnTriggerExit (Collider col){
		jumpInd = false;
	}
}
