﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyThrowHint : MonoBehaviour
{

    public GameObject Hint;
    bool showed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "RightGrab")
        {
            if (!showed)
            {
                Hint.SetActive(true);
                showed = true;
            }
        }
        if (col.gameObject.name == "LefttGrab")
        {
            if (!showed)
            {
                Hint.SetActive(true);
                showed = true;
            }
        }
    }
}
