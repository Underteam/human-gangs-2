﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level7Cut1 : MonoBehaviour
{

    public GameObject Unicorn;
    public GameObject CutCam;
    public bool End;
    public GameObject Boss;
    public GameObject Boss1;
    public GameObject Cannon;
    public GameObject Cannon1;
    public GameObject PlayerFallCol;
    public GameObject Hp;
    public GameObject Luster;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (End)
        {
            Unicorn.GetComponent<Animator>().enabled = false;
            Boss.GetComponent<Boss>().isFire = true;
            Boss1.GetComponent<Boss>().isFire = true;
            Cannon.GetComponent<Animator>().enabled = true;
            Cannon1.GetComponent<Animator>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = true;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = true;
            Hp.SetActive(true);
            Luster.GetComponent<AudioSource>().enabled = true;
            CutCam.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "ColPlayer")
        {
            Unicorn.GetComponent<Animator>().enabled = true;
            CutCam.SetActive(true);
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[0].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[1].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[2].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[3].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[4].GetComponent<Image>().enabled = false;
            PlayerFallCol.GetComponent<PlayerFall>().PlayerControl[5].GetComponent<Image>().enabled = false;
            Hp.SetActive(false);
        }
    }
}
