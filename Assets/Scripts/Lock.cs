﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    public GameObject Gate1;
    public GameObject Gate2;
    public GameObject Bridge;
    public GameObject Canister;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Throwing"))
        {
            if (col.gameObject.name != "canister")
            {
                gameObject.GetComponent<Animator>().enabled = true;
                StartCoroutine(Gates());
                Destroy(gameObject, 0.6f);
            }
        }
    }
    public IEnumerator Gates()
    {
        yield return new WaitForSeconds(0.5f);
        Gate1.GetComponent<Animator>().enabled = true;
        Gate2.GetComponent<Animator>().enabled = true;
        Canister.GetComponent<Rigidbody>().AddForce(Canister.transform.up * 8, ForceMode.Impulse);
        Bridge.GetComponent<Animator>().enabled = true;
    }
}
