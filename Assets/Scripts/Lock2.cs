﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock2 : MonoBehaviour
{

    public GameObject Key1;
    public GameObject Key2;
    public GameObject key1;
    public GameObject key2;
    int Keys;
    public GameObject Gates;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "key (1)")
        {
            Destroy(key1);
            Key1.SetActive(true);
            Keys++;
            if (Keys >= 2)
            {
                StartCoroutine(Open());
            }
        }
        if (col.gameObject.name == "key (2)")
        {
            Destroy(key2);
            Key2.SetActive(true);
            Keys++;
            if(Keys >= 2)
            {
                StartCoroutine(Open());
            }
        }
    }
    public IEnumerator Open()
    {
        yield return new WaitForSeconds(1.5f);
        Gates.GetComponent<Animator>().enabled = true;
        Enemy1.SetActive(true);
        Enemy2.SetActive(true);
        Enemy3.SetActive(true);
        Destroy(gameObject);
    }
}
