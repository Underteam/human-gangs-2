﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Luster : MonoBehaviour
{

    public GameObject Player;
    public GameObject Circle;
    public GameObject Cage;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Rigidbody>().velocity.magnitude >= Speed)
        {
            Circle.transform.gameObject.tag = "Luster";
            Cage.transform.gameObject.tag = "Luster";
        }
        else
        {
            Circle.transform.gameObject.tag = "Untagged";
            Cage.transform.gameObject.tag = "Untagged";
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("PlayerFists"))
        {
            GetComponent<Rigidbody>().AddForce(Player.transform.forward * 25, ForceMode.Impulse);
            //transform.gameObject.tag = "Luster";
            //StartCoroutine(tagDelay());
        }
    }
    public IEnumerator tagDelay()
    {
        yield return new WaitForSeconds(8f);
        transform.gameObject.tag = "Untagged";
    }
}
