﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Advertisements;
//using GoogleMobileAds.Api;
using UnityEngine.UI;
using System.IO;

public class Menu : MonoBehaviour {

	public GameObject pauseMenu;
	public GameObject Controls;
	public GameObject Loading;
	public GameObject Camera;
	public GameObject Player;
	public int levelmanager;
	private const string banner = "";
	private const string interstitial = "";
	//private InterstitialAd ad;
	public GameObject Tutor;
	public GameObject Tutor1;
	public int Music = 1;
	public GameObject MusicSource;
	public GameObject MusicOffImg;
	public int targetScore;
	public int Score;
	public GameObject winPan;
    public bool Ad = false;

	// Use this for initialization
	void Start () {
		if (Advertisement.isSupported)
			Advertisement.Initialize ("3361167", false);
		if(PlayerPrefs.HasKey("levelmanager"))
			levelmanager = PlayerPrefs.GetInt ("levelmanager");
		if(PlayerPrefs.HasKey("Music"))
			Music = PlayerPrefs.GetInt ("Music");
        //      BannerView bannerV = new BannerView(banner, AdSize.Banner, AdPosition.Top);
        //      AdRequest request = new AdRequest.Builder().Build();
        //      bannerV.LoadAd(request);
        if (Controls != null) Controls.SetActive(false);
        if (Camera != null) Camera.SetActive(false);
        if (Player != null) Player.SetActive(false);
        StartCoroutine(startFixDelay());

		StartCoroutine(SendAppOpened());
    }

	public IEnumerator SendAppOpened()
	{
		yield return null;
		AppsFlyerSDK.AppsFlyer.sendEvent("af_app_opened", new Dictionary<string, string>());
	}

	public void OnAdLoaded(object sender, System.EventArgs args){
		//ad.Show ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.L))
			PlayerPrefs.DeleteAll ();

		if (Music == 0)
		{
			if (MusicSource != null) MusicSource.GetComponent<AudioSource>().enabled = false;
		}
		else
		{
			if (MusicSource != null) MusicSource.GetComponent<AudioSource>().enabled = true;
		}

		if (Music == 1)
			MusicOffImg.SetActive (false);
		
		if (Music == 0)
			MusicOffImg.SetActive (true);
		
		if (targetScore == Score)
			StartCoroutine (WinDelay ());
        
		if (Ad)
        {
            ShowAd();
        }
	}

	public IEnumerator WinDelay(){
		yield return new WaitForSeconds (3f);
		winPan.SetActive (true);
	}

	public void cliclStart (){
		Loading.SetActive (true);
		StartCoroutine (startDelay ());
        if (levelmanager != 0)
        {
            if (Advertisement.IsReady("video"))
            {
                Advertisement.Show("video");
            }
            //else
            //{
            //    ad = new InterstitialAd(interstitial);
            //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
            //    ad.LoadAd(request);
            //    ad.OnAdLoaded += OnAdLoaded;
            //}
        }
    }
    public void ShowAd()
    {
        if (Advertisement.IsReady("video"))
        {
            Advertisement.Show("video");
        }
        //else
        //{
        //    ad = new InterstitialAd(interstitial);
        //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
        //    ad.LoadAd(request);
        //    ad.OnAdLoaded += OnAdLoaded;
        //}
    }

    public void MusicOnOff (){
		if (Music == 1) {
			Music = 0;
//			MusicOffImg.SetActive (true);
		} else {
			Music = 1;
//			MusicOffImg.SetActive (false);
		}
		PlayerPrefs.SetInt ("Music", Music);
		PlayerPrefs.Save ();
	}

	public void nextLevel (){
		Controls.SetActive (false);
		Loading.SetActive (true);
		StartCoroutine (nextDelay ());
		if (Advertisement.IsReady ("video")) {
			Advertisement.Show ("video");
        }
        //else
        //{
        //    ad = new InterstitialAd(interstitial);
        //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
        //    ad.LoadAd(request);
        //    ad.OnAdLoaded += OnAdLoaded;
        //}
    }
	public void Level1 (){
		Loading.SetActive (true);
		StartCoroutine (level1Delay ());
		if (Advertisement.IsReady ("video")) {
			Advertisement.Show ("video");
        }
        //else
        //{
        //    ad = new InterstitialAd(interstitial);
        //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
        //    ad.LoadAd(request);
        //    ad.OnAdLoaded += OnAdLoaded;
        //}
    }
    public void Level2()
    {
        Loading.SetActive(true);
        StartCoroutine(level2Delay());
        if (Advertisement.IsReady("video"))
        {
            Advertisement.Show("video");
        }
        //else
        //{
        //    ad = new InterstitialAd(interstitial);
        //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
        //    ad.LoadAd(request);
        //    ad.OnAdLoaded += OnAdLoaded;
        //}
    }
    public void LevelRestore()
    {
        levelmanager = 0;
        PlayerPrefs.SetInt("levelmanager", levelmanager);
        PlayerPrefs.Save();
    }

	public IEnumerator startDelay(){
		yield return new WaitForSeconds (0.1f);
		if (levelmanager == 0)
			Application.LoadLevel (1);
		if (levelmanager == 1)
			Application.LoadLevel (2);
		if (levelmanager == 2)
			Application.LoadLevel (3);
		if (levelmanager == 3)
			Application.LoadLevel (4);
		if (levelmanager == 4)
			Application.LoadLevel (5);
		if (levelmanager == 5)
			Application.LoadLevel (6);
		if (levelmanager == 6)
			Application.LoadLevel (7);
        if (levelmanager == 7)
            Application.LoadLevel(8);
    }
	public IEnumerator nextDelay(){
		yield return new WaitForSeconds (0.1f);
		levelmanager++;
		if (levelmanager >= 8)
			levelmanager = 0;
		PlayerPrefs.SetInt ("levelmanager", levelmanager);
		PlayerPrefs.Save ();
		Application.LoadLevel(Application.loadedLevel +1);
	}
	public IEnumerator level1Delay(){
		yield return new WaitForSeconds (0.1f);
		levelmanager = 0;
		PlayerPrefs.SetInt ("levelmanager", levelmanager);
		PlayerPrefs.Save ();
		Application.LoadLevel(1);
	}
    public IEnumerator level2Delay()
    {
        yield return new WaitForSeconds(0.1f);
        levelmanager = 2;
        PlayerPrefs.SetInt("levelmanager", levelmanager);
        PlayerPrefs.Save();
        Application.LoadLevel(3);
    }

    public IEnumerator startFixDelay()
	{
		yield return new WaitForSeconds (0.01f);
		if (Controls != null) Controls.SetActive (true);
		if (Camera != null) Camera.SetActive (true);
		if (Player != null) Player.SetActive (true);
	}

	public void Pause ()
	{
		if (Controls != null) Controls.SetActive (false);
		pauseMenu.SetActive (true);
		Time.timeScale = 0;
	}
	public void Resume (){
		Time.timeScale = 1;
		pauseMenu.SetActive (false);
		if (Controls != null) Controls.SetActive (true);
	}
	public void MoreGamesAndroid (){
		Application.OpenURL ("https://play.google.com/store/apps/developer?id=Charming+Agency");
	}
	public void MoreGamesIOS (){
		Application.OpenURL ("https://itunes.apple.com/gb/developer/marina-sharova/id1453152670");
	}
	public void PrivacyPolicy (){
		Application.OpenURL ("https://sites.google.com/view/agency-privacy-policy/");
	}

    public void Twitter()
    {
        Application.OpenURL("https://twitter.com/AgencyCharming");
    }
    public void Reddit()
    {
        Application.OpenURL("https://www.reddit.com/r/CharmingAgency/");
    }
    public void Facebook()
    {
        Application.OpenURL("https://www.facebook.com/groups/293670277985668/");
    }
    public void Vk()
    {
        Application.OpenURL("https://vk.com/club189488107");
    }

    public void ToMenu (){
		Time.timeScale = 1;
		Application.LoadLevel (0);
	}
	public void Exit (){
		Application.Quit ();
	}
	public void Restart (){
		Loading.SetActive (true);
		StartCoroutine (restartDelay ());
		if (Advertisement.IsReady ("video")) {
			Advertisement.Show ("video");
        }
        //else
        //{
        //    ad = new InterstitialAd(interstitial);
        //    AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).AddTestDevice("").Build();
        //    ad.LoadAd(request);
        //    ad.OnAdLoaded += OnAdLoaded;
        //}
    }
	public IEnumerator restartDelay(){
		yield return new WaitForSeconds (0.1f);
		Application.LoadLevel(Application.loadedLevel);
	}
	public void TutorOff (){
		Tutor.SetActive (false);
	}
	public void Tutor1Off (){
		Tutor1.SetActive (false);
	}
}
