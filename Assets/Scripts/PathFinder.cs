﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour {

	public GameObject Waypoint;
	GameObject Player;
	public GameObject Waypoints;
	public GameObject Enemy1;
	public GameObject Enemy2;
	public GameObject Enemy3;
    public GameObject Root;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter (Collider col){
		if (col.gameObject.name == "ColPlayer") {
   //         Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
   //         Waypoint.transform.position = Player.transform.position;
			//Waypoint.transform.parent = Player.transform;
		}
//		if (col.CompareTag ("ColEnemy1")) {
//			Waypoint.transform.position = Enemy1.transform.position;
//			Waypoint.transform.parent = Enemy1.transform;
//		}
//		if (col.CompareTag ("ColEnemy2")) {
//			Waypoint.transform.position = Enemy2.transform.position;
//			Waypoint.transform.parent = Enemy2.transform;
//		}
//		if (col.CompareTag ("ColEnemy3")) {
//			Waypoint.transform.position = Enemy3.transform.position;
//			Waypoint.transform.parent = Enemy3.transform;
//		}
	}
	private void OnTriggerStay (Collider col){
		if (col.gameObject.name == "ColPlayer") {
            Player = GameObject.FindGameObjectWithTag("PlayerFixCoords");
            Waypoint.transform.position = Player.transform.position;
            Waypoint.transform.parent = Player.transform;
        }
	}
	private void OnTriggerExit (Collider col){
		if (col.gameObject.name == "ColPlayer") {
			Waypoint.transform.parent = Root.transform;
			StartCoroutine (WaypointDelay ());
		}
//		if (col.CompareTag ("ColEnemy1")) {
//			Waypoint.transform.parent = Waypoints.transform;
//			StartCoroutine (WaypointDelay ());
//		}
//		if (col.CompareTag ("ColEnemy2")) {
//			Waypoint.transform.parent = Waypoints.transform;
//			StartCoroutine (WaypointDelay ());
//		}
//		if (col.CompareTag ("ColEnemy3")) {
//			Waypoint.transform.parent = Waypoints.transform;
//			StartCoroutine (WaypointDelay ());
//		}
	}
	public IEnumerator WaypointDelay(){
		yield return new WaitForSeconds (0.001f);
		Waypoint.transform.position = gameObject.transform.position;
	}
}
