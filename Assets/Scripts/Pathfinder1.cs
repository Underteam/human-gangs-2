﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder1 : MonoBehaviour {

	public GameObject Waypoint;
	public GameObject Player;
	public GameObject Waypoints;
	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject leftDirection;
	public GameObject rightDirection;
	public float hitCounter;
	public float hitIndex;
	public GameObject rightHitbox;
	public GameObject leftHitbox;
    public GameObject Root;
    public GameObject Col;
//	private int HandSwitch = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
//		Waypoint.transform.position = gameObject.transform.position;
	}

    public IEnumerator Coltrue()
    {
        while (true)
        {
            Col.GetComponent<SphereCollider>().enabled = true;
            yield return new WaitForSeconds(1);
        }
    }

    private void OnTriggerStay (Collider col){
		if (col.gameObject.name == "ColPlayer") {
            Col.GetComponent<SphereCollider>().enabled = false;
			Waypoint.transform.parent = Root.transform;
			StartCoroutine (WaypointDelay ());
			Hit ();
//			Waypoint.transform.position = new Vector3 (gameObject.transform.position.x, 0, gameObject.transform.position.z);
		}
        if (col.gameObject.name == "ColliderForStop")
        {
            Col.GetComponent<SphereCollider>().enabled = false;
            Waypoint.transform.parent = Root.transform;
            StartCoroutine(WaypointDelay());
            Hit();
            //			Waypoint.transform.position = new Vector3 (gameObject.transform.position.x, 0, gameObject.transform.position.z);
        }
        //		if (col.CompareTag ("ColEnemy1")) {
        //			Waypoint.transform.parent = Waypoints.transform;
        //			StartCoroutine (WaypointDelay ());
        //			Hit ();
        //		}
        //		if (col.CompareTag ("ColEnemy2")) {
        //			Waypoint.transform.parent = Waypoints.transform;
        //			StartCoroutine (WaypointDelay ());
        //			Hit ();
        //		}
        //		if (col.CompareTag ("ColEnemy3")) {
        //			Waypoint.transform.parent = Waypoints.transform;
        //			StartCoroutine (WaypointDelay ());
        //			Hit ();
        //		}
    }
	public IEnumerator WaypointDelay(){
		yield return new WaitForSeconds (0.001f);
		Waypoint.transform.position = gameObject.transform.position;
	}
	private void OnTriggerExit (Collider col){
		if (col.gameObject.name == "ColPlayer") {
            Col.GetComponent<SphereCollider>().enabled = true;
            Waypoint.transform.position = Player.transform.position;
			Waypoint.transform.parent = Player.transform;
		}
        if (col.gameObject.name == "ColliderForStop")
        {
            Col.GetComponent<SphereCollider>().enabled = true;
            Waypoint.transform.position = Player.transform.position;
            Waypoint.transform.parent = Player.transform;
        }
        //		if (col.CompareTag ("ColEnemy1")) {
        //			Waypoint.transform.position = Player.transform.position;
        //			Waypoint.transform.parent = Player.transform;
        //		}
        //		if (col.CompareTag ("ColEnemy2")) {
        //			Waypoint.transform.position = Player.transform.position;
        //			Waypoint.transform.parent = Player.transform;
        //		}
        //		if (col.CompareTag ("ColEnemy3")) {
        //			Waypoint.transform.position = Player.transform.position;
        //			Waypoint.transform.parent = Player.transform;
        //		}
    }
	private void Hit (){
		if (hitCounter <= 0) {
			hitCounter = hitIndex;
			rightHand.GetComponent <Rigidbody> ().AddForce (rightDirection.transform.forward * 20, ForceMode.Impulse);
			rightHitbox.GetComponent <BoxCollider> ().enabled = true;
			StartCoroutine (rightHitboxDelay ());
			StartCoroutine (leftHandDelay ());
		} else
			hitCounter -= Time.deltaTime;
	}
	public IEnumerator leftHandDelay(){
		yield return new WaitForSeconds (0.25f);
		leftHand.GetComponent <Rigidbody> ().AddForce (leftDirection.transform.forward * 20, ForceMode.Impulse);
		leftHitbox.GetComponent <BoxCollider> ().enabled = true;
		StartCoroutine (leftHitboxDelay ());
	}
	public IEnumerator rightHitboxDelay(){
		yield return new WaitForSeconds (0.25f);
		rightHitbox.GetComponent <BoxCollider> ().enabled = false;
	}
	public IEnumerator leftHitboxDelay(){
		yield return new WaitForSeconds (0.25f);
		leftHitbox.GetComponent <BoxCollider> ().enabled = false;
	}
}
