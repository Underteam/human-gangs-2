﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PickedItem : MonoBehaviour {

	public GameObject leftHand;
	public GameObject rightHand;
	public GameObject playerController;
//	int indexPick;
	public GameObject Grabbed;
	public GameObject Player;
    public GameObject Thrower;
    public GameObject Aim;
    public int picked;
    GameObject TargetCam;
    public GameObject Explosion;
    GameObject explosion;

	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
//		indexPick = playerController.GetComponent <PlayerController> ().pickIndex;
	}

	private void OnTriggerEnter (Collider col){
		if (col.gameObject.name == "RightGrab") {
            if (picked < 1)
            {
                picked += 1;
                GetComponent<Rigidbody>().isKinematic = true;
                //			GetComponent <BoxCollider> ().isTrigger = true;
                GetComponent<MeshCollider>().isTrigger = true;
                transform.parent = rightHand.transform;
                Aim.SetActive(true);
                TargetCam = GameObject.Find("camera target 1");
                TargetCam.transform.localPosition = new Vector3(0, 0.5f, -0.4f);
                Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 200;
                Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 50;
            }
		}
		if (col.gameObject.name == "LeftGrab") {
            if (picked < 1)
            {
                picked += 1;
                GetComponent<Rigidbody>().isKinematic = true;
                //			GetComponent <BoxCollider> ().isTrigger = true;
                GetComponent<MeshCollider>().isTrigger = true;
                transform.parent = leftHand.transform;
                Aim.SetActive(true);
                TargetCam = GameObject.Find("camera target 1");
                TargetCam.transform.localPosition = new Vector3(0, 0.5f, -0.4f);
                Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 200;
                Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 50;
            }
        }
		if (col.gameObject.name == "LeftThrow") {
			transform.parent = Grabbed.transform;
			GetComponent <Rigidbody> ().isKinematic = false;
//			GetComponent <BoxCollider> ().isTrigger = false;
			GetComponent <MeshCollider> ().isTrigger = false;
            transform.position = Thrower.transform.position;
            transform.rotation = Thrower.transform.rotation;
			GetComponent <Rigidbody> ().AddForce (Thrower.transform.forward * 18, ForceMode.Impulse);
			transform.gameObject.tag = "Throwing";
            Aim.SetActive(false);
            TargetCam = GameObject.Find("camera target 1");
            TargetCam.transform.localPosition = new Vector3(0, -0.1f, 0);
            Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 360;
            Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 180;
            picked = 0;
            StartCoroutine (tagDelay ());
//			GetComponent <Rigidbody> ().AddForce (Player.transform.up * 1, ForceMode.Impulse);
		}
		if (col.gameObject.name == "RightThrow") {
			transform.parent = Grabbed.transform;
			GetComponent <Rigidbody> ().isKinematic = false;
//			GetComponent <BoxCollider> ().isTrigger = false;
			GetComponent <MeshCollider> ().isTrigger = false;
            transform.position = Thrower.transform.position;
            transform.rotation = Thrower.transform.rotation;
            GetComponent <Rigidbody> ().AddForce (Thrower.transform.forward * 18, ForceMode.Impulse);
			transform.gameObject.tag = "Throwing";
            Aim.SetActive(false);
            TargetCam = GameObject.Find("camera target 1");
            TargetCam.transform.localPosition = new Vector3(0, -0.1f, 0);
            Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 360;
            Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 180;
            picked = 0;
            StartCoroutine (tagDelay ());
//			GetComponent <Rigidbody> ().AddForce (Player.transform.up * 1, ForceMode.Impulse);
		}

        if (col.gameObject.name == "LeftPut" | col.gameObject.name == "RightPut")
        {
            transform.parent = Grabbed.transform;
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<MeshCollider>().isTrigger = false;
            transform.position = Thrower.transform.position;
            transform.rotation = Thrower.transform.rotation;
            Aim.SetActive(false);
            TargetCam = GameObject.Find("camera target 1");
            TargetCam.transform.localPosition = new Vector3(0, -0.1f, 0);
            Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 360;
            Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 180;
            picked = 0;
            StartCoroutine(tagDelay());
        }
        if (col.CompareTag("Ground"))
        {
            transform.gameObject.tag = "Untagged";
        }
        if (col.gameObject.name == "Cannon")
        {
            explosion = Instantiate(Explosion, transform.position, transform.rotation);
            Destroy(explosion, 2);
            transform.position = new Vector3(0, 4, 50);
        }
    }
	public IEnumerator tagDelay(){
		yield return new WaitForSeconds (4f);
		transform.gameObject.tag = "Untagged";
	}
    public IEnumerator tag2Delay()
    {
        yield return new WaitForSeconds(1f);
        transform.gameObject.tag = "Untagged";
    }
}
