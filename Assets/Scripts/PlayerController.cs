﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerController : MonoBehaviour {

	public float forcePower;
	public GameObject MoveBone;
	public GameObject rightHand;
	public GameObject leftHand;
	private int HandSwitch = 1;
	public GameObject Player;
	public GameObject rightDirection;
	public GameObject leftDirection;
	public GameObject Grabbed1;
	public GameObject Grabbed2;
	public GameObject Grabbed3;
	public GameObject Grabbed4;
	public GameObject Grabbed5;
	public GameObject Grabbed6;
	public GameObject Grabbed7;
	public GameObject Grabbed8;
	public GameObject Grabbed9;
	public GameObject Grabbed10;
	public GameObject grabbedHips;
	public GameObject rightLeg;
	public GameObject leftLeg;
	protected Animator _animatorPlayer;
	Animator animatorPlayer;
	public GameObject fixCoords;
	private float walkCounter;
	public GameObject rightHitbox;
	public GameObject leftHitbox;
	public int pickIndex;
	public GameObject leftGrab;
	public GameObject rightGrab;
	public GameObject leftThrow;
	public GameObject rightThrow;
    public GameObject leftPut;
    public GameObject rightPut;
    public GameObject Enemy1;
	public GameObject Enemy2;
	public GameObject Enemy3;
	public GameObject Enemy4;
	public GameObject Enemy5;
	public GameObject Enemy6;
	public GameObject Enemy7;
	public GameObject Enemy8;
	public GameObject Enemy9;
	public GameObject Enemy10;

	public GameObject enemy1Colforhit;
	public GameObject enemy2Colforhit;
	public GameObject enemy3Colforhit;
	public GameObject enemy4Colforhit;
	public GameObject enemy5Colforhit;
	public GameObject enemy6Colforhit;
	public GameObject enemy7Colforhit;
	public GameObject enemy8Colforhit;
	public GameObject enemy9Colforhit;
	public GameObject enemy10Colforhit;

    public GameObject PlayerColForHit;
    public GameObject Aim;
    GameObject TargetCam;
    bool Hitable = true;
    public GameObject WhooshSnd;
    GameObject whooshSnd;

	// Use this for initialization
	void Start () {
		animatorPlayer = fixCoords.GetComponent <Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		Physics.IgnoreLayerCollision (9, 10);
		if (animatorPlayer.GetFloat ("Forward") > 0.1f) {
			if (walkCounter <= 0) {
				walkCounter = 0.6f;
				rightLeg.GetComponent <Rigidbody> ().AddForce (fixCoords.transform.forward * 8, ForceMode.Impulse);
                leftLeg.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.forward * 8, ForceMode.Impulse);
                StartCoroutine (rightLegBack ());
				StartCoroutine (leftLegDelay ());
                leftHand.GetComponent<Rigidbody>().AddForce(fixCoords.transform.forward * 4, ForceMode.Impulse);
                leftHand.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.right * 2, ForceMode.Impulse);
                rightHand.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.forward * 4, ForceMode.Impulse);
                rightHand.GetComponent<Rigidbody>().AddForce(fixCoords.transform.right * 2, ForceMode.Impulse);
                StartCoroutine(rightHandDelay());
                StartCoroutine(leftHandBack());
            } else
				walkCounter -= Time.deltaTime;
//			if (walkCounter <= 0f) {
//				walkCounter = 0.3f;
//				leftLeg.GetComponent <Rigidbody> ().AddForce (transform.up * 15, ForceMode.Impulse);
//			} else
//				walkCounter -= Time.deltaTime;
		}
        if (Input.GetKeyDown(KeyCode.E))
        {
            Hit();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Pick();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            put();
        }
    }

	public IEnumerator leftLegDelay(){
		yield return new WaitForSeconds (0.3f);
		leftLeg.GetComponent <Rigidbody> ().AddForce (fixCoords.transform.forward * 8, ForceMode.Impulse);
		//StartCoroutine (leftLegBack ());
	}
	public IEnumerator rightLegBack(){
		yield return new WaitForSeconds (0.3f);
		rightLeg.GetComponent <Rigidbody> ().AddForce (-1 * fixCoords.transform.forward * 8, ForceMode.Impulse);
	}
	//public IEnumerator leftLegBack(){
	//	yield return new WaitForSeconds (0.3f);
	//	leftLeg.GetComponent <Rigidbody> ().AddForce (-1 * fixCoords.transform.forward * 8, ForceMode.Impulse);
	//}
    //
    public IEnumerator rightHandDelay()
    {
        yield return new WaitForSeconds(0.3f);
        rightHand.GetComponent<Rigidbody>().AddForce(fixCoords.transform.forward * 4, ForceMode.Impulse);
        rightHand.GetComponent<Rigidbody>().AddForce(fixCoords.transform.right * 2, ForceMode.Impulse);
        //StartCoroutine(rightHandBack());
    }
    public IEnumerator leftHandBack()
    {
        yield return new WaitForSeconds(0.3f);
        leftHand.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.forward * 4, ForceMode.Impulse);
        leftHand.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.right * 2, ForceMode.Impulse);
    }
    //public IEnumerator rightHandBack()
    //{
    //    yield return new WaitForSeconds(0.3f);
    //    rightHand.GetComponent<Rigidbody>().AddForce(-1 * fixCoords.transform.forward * 8, ForceMode.Impulse);
    //}
    //
    public void Hit (){
        if (Hitable)
        {
            HandSwitch += 1;
            if (HandSwitch >= 2)
                HandSwitch = 0;
            if (HandSwitch == 0)
            {
                rightHand.GetComponent<Rigidbody>().AddForce(rightDirection.transform.forward * forcePower, ForceMode.Impulse);
                rightHitbox.GetComponent<BoxCollider>().enabled = true;
                StartCoroutine(rightHitboxDelay());
                whooshSnd = Instantiate(WhooshSnd, Player.transform.position, Player.transform.rotation);
                Destroy(whooshSnd, 1);
            }
            if (HandSwitch == 1)
            {
                leftHand.GetComponent<Rigidbody>().AddForce(leftDirection.transform.forward * forcePower, ForceMode.Impulse);
                leftHitbox.GetComponent<BoxCollider>().enabled = true;
                StartCoroutine(leftHitboxDelay());
                whooshSnd = Instantiate(WhooshSnd, Player.transform.position, Player.transform.rotation);
                Destroy(whooshSnd, 1);
            }
            if (!Player.GetComponent<ThirdPersonCharacter>().m_IsGrounded)
            {
                PlayerColForHit.GetComponent<PlayerFall>().AirAttack();
                Hitable = false;
                StartCoroutine(HitableDelay());
                whooshSnd = Instantiate(WhooshSnd, Player.transform.position, Player.transform.rotation);
                Destroy(whooshSnd, 1);
            }
        }
	}
    public IEnumerator HitableDelay(){
		yield return new WaitForSeconds (2.3f);
        Hitable = true;
    }
	public IEnumerator rightHitboxDelay(){
		yield return new WaitForSeconds (0.5f);
		rightHitbox.GetComponent <BoxCollider> ().enabled = false;
	}
	public IEnumerator leftHitboxDelay(){
		yield return new WaitForSeconds (0.5f);
		leftHitbox.GetComponent <BoxCollider> ().enabled = false;
	}

	public void Pick (){
		if (pickIndex == 0) {
			pickIndex = 1;
			rightHand.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * forcePower, ForceMode.Impulse);
			leftHand.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * forcePower, ForceMode.Impulse);
			rightHand.GetComponent <ConstantForce> ().force = new Vector3 (0, 100, 0);
			leftHand.GetComponent <ConstantForce> ().force = new Vector3 (0, 100, 0);
			leftGrab.SetActive (true);
			rightGrab.SetActive (true);
			StartCoroutine (grabDelay ());
//		StartCoroutine (pickDelay ());
//			Grabbed.SetActive (true);
		}else if (pickIndex == 1) {
			pickIndex = 0;
			rightHand.GetComponent <ConstantForce> ().force = new Vector3 (0, 0, 0);
			leftHand.GetComponent <ConstantForce> ().force = new Vector3 (0, 0, 0);
			rightHand.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * forcePower, ForceMode.Impulse);
			leftHand.GetComponent <Rigidbody> ().AddForce (Player.transform.forward * forcePower, ForceMode.Impulse);
			leftThrow.SetActive (true);
			rightThrow.SetActive (true);
            Aim.SetActive(false);
            TargetCam = GameObject.Find("camera target 1");
            TargetCam.transform.localPosition = new Vector3(0, -0.1f, 0);
            Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 360;
            Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 180;
            StartCoroutine (grabDelay ());
//			Grabbed.SetActive (false);
		}
	}
    public void put()
    {
        if (pickIndex == 1)
        {
            pickIndex = 0;
            rightHand.GetComponent<ConstantForce>().force = new Vector3(0, 0, 0);
            leftHand.GetComponent<ConstantForce>().force = new Vector3(0, 0, 0);
            rightHand.GetComponent<Rigidbody>().AddForce(Player.transform.forward * 5, ForceMode.Impulse);
            leftHand.GetComponent<Rigidbody>().AddForce(Player.transform.forward * 5, ForceMode.Impulse);
            leftPut.SetActive(true);
            rightPut.SetActive(true);
            Aim.SetActive(false);
            TargetCam = GameObject.Find("camera target 1");
            TargetCam.transform.localPosition = new Vector3(0, -0.1f, 0);
            Player.GetComponent<ThirdPersonCharacter>().m_MovingTurnSpeed = 360;
            Player.GetComponent<ThirdPersonCharacter>().m_StationaryTurnSpeed = 180;
            StartCoroutine(grabDelay());
        }
    }
	public IEnumerator grabDelay(){
		yield return new WaitForSeconds (0.7f);
		leftGrab.SetActive (false);
		rightGrab.SetActive (false);
		leftThrow.SetActive (false);
		rightThrow.SetActive (false);
        leftPut.SetActive(false);
        rightPut.SetActive(false);
    }
	public void throwEnemy (){
		if (Grabbed1.activeInHierarchy == true) {
			Enemy1.SetActive (true);
			enemy1Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed2.activeInHierarchy == true) {
			Enemy2.SetActive (true);
			enemy2Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed3.activeInHierarchy == true) {
			Enemy3.SetActive (true);
			enemy3Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed4.activeInHierarchy == true) {
			Enemy4.SetActive (true);
			enemy4Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed5.activeInHierarchy == true) {
			Enemy5.SetActive (true);
			enemy5Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed6.activeInHierarchy == true) {
			Enemy6.SetActive (true);
			enemy6Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed7.activeInHierarchy == true) {
			Enemy7.SetActive (true);
			enemy7Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed8.activeInHierarchy == true) {
			Enemy8.SetActive (true);
			enemy8Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (Grabbed9.activeInHierarchy == true) {
			Enemy9.SetActive (true);
			enemy9Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		//if (Grabbed10.activeInHierarchy == true) {
		//	Enemy10.SetActive (true);
		//	enemy10Colforhit.GetComponent <EnemyFall> ().Throw();
		//}
	}
}
