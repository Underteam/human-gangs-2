﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Advertisements;
//using GoogleMobileAds.Api;
using UnityEngine.UI;
using System.IO;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Vehicles.Car;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Cameras;

public class PlayerFall : MonoBehaviour {

    public GameObject hips;
    public GameObject Head;
    public GameObject fixCoords;
    public GameObject Col;
    public GameObject Player;
    public GameObject leftHand;
    public GameObject rightHand;
    public GameObject playerControl;
    public GameObject hitEffect;
    GameObject hiteffect;
    public int Hp;
    public int maxHp;
    public float RegenSpeed;
    float HpIndex;
    public GameObject HpBar;
    public GameObject losePan;
    public GameObject winPan;
    public GameObject Controls;
    public GameObject Tutor;
    public GameObject Tutor1;
    public GameObject tutorTrigger;
    public GameObject tutorTrigger1;
    public GameObject AirAttackCol;
    public GameObject Car;
    public GameObject carCam;
    public GameObject PlayerCam;
    public GameObject carExitBtn;
    public GameObject[] PlayerControl;
    public GameObject[] CarControl;
    public GameObject Driver;
    public GameObject HidePlayer;
    public GameObject carExit;
    public GameObject[] PlayerPart;
    public int BossDamage;
    int FallRandom;
    //	public GameObject enemyController;

    // Use this for initialization
    void Start () {
		HpIndex = 1f / Hp;
        PlayerControl[0].GetComponent<Image>().enabled = true;
        PlayerControl[1].GetComponent<Image>().enabled = true;
        PlayerControl[2].GetComponent<Image>().enabled = true;
        PlayerControl[3].GetComponent<Image>().enabled = true;
        PlayerControl[4].GetComponent<Image>().enabled = true;
        PlayerControl[5].GetComponent<Image>().enabled = true;
        CarControl[0].GetComponent<Image>().enabled = false;
        CarControl[1].GetComponent<Image>().enabled = false;
        CarControl[2].GetComponent<Image>().enabled = false;
        CarControl[3].GetComponent<Image>().enabled = false;
        CarControl[4].GetComponent<Image>().enabled = false;
        StartCoroutine(Regen());
    }
	
	// Update is called once per frame
	void Update () {
		Head.transform.localRotation = Quaternion.Euler (0, 0, 0);
		HpBar.GetComponent <Scrollbar> ().size = Hp * HpIndex;
		if (Hp <= 0) {
			HpBar.SetActive (false);
		}
        if (Hp >= maxHp)
        {
            Hp = maxHp;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            exitCar();
        }
    }

    public IEnumerator Regen()
    {
        while (true)
        {
            Hp++;
            yield return new WaitForSeconds(RegenSpeed);
        } 
    }

    private void OnTriggerEnter (Collider col){
		if (col.CompareTag ("EnemyFists")) {
			Hp--;
			hiteffect = Instantiate (hitEffect, new Vector3 (transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
			if (Hp <= 0) {
				Fall();
			}
			Destroy (hiteffect, 2);
		}
        if (col.CompareTag ("BossFists")) {
			Hp -= BossDamage;
			hiteffect = Instantiate (hitEffect, new Vector3 (transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
            //FallRandom = UnityEngine.Random.Range(0, 5);
            //if (FallRandom == 4)
            //{
            //    Fall();
            //}
            if (Hp <= 0) {
				Fall();
			}
			Destroy (hiteffect, 2);
		}
        if (col.CompareTag("BulletForPlayer"))
        {
            Hp-=10;
            hiteffect = Instantiate(hitEffect, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z), transform.rotation);
            Fall();
            Destroy(hiteffect, 2);
        }
        if (col.gameObject.name == "FallTrigger") {
			Hp = 0;
			losePan.SetActive (true);
			Controls.SetActive (false);
			hips.GetComponent <PlayerFixCoords> ().enabled = false;
			Head.GetComponent <ConstantForce> ().enabled = false;
			rightHand.GetComponent <ConstantForce> ().enabled = false;
			leftHand.GetComponent <ConstantForce> ().enabled = false;
			Col.GetComponent <CapsuleCollider> ().enabled = false;
			Player.GetComponent <Animator> ().enabled = false;
			gameObject.GetComponent <CapsuleCollider> ().enabled = false;
			Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
			hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
            PlayerCam.GetComponent<FreeLookCam>().enabled = false;
		}
		if (col.gameObject.name == "WinTrigger") {
			winPan.SetActive (true);
			Controls.SetActive (false);
		}
		if (col.gameObject.name == "TutorTrigger") {
			Tutor.SetActive (true);
			tutorTrigger.SetActive (false);
		}
		if (col.gameObject.name == "TutorTrigger (1)") {
			Tutor1.SetActive (true);
			tutorTrigger1.SetActive (false);
		}
        if (col.gameObject.name == "CarEnterCol")
        {
            if (playerControl.GetComponent<PlayerController>().pickIndex == 0)
            {
                Player.transform.position = new Vector3(Car.transform.position.x, Car.transform.position.y + 0.9f, Car.transform.position.z);
                PlayerPart[0].SetActive(false);
                PlayerPart[1].SetActive(false);
                PlayerPart[2].SetActive(false);
                PlayerPart[3].SetActive(false);
                PlayerPart[4].SetActive(false);
                PlayerPart[5].SetActive(false);
                PlayerPart[6].SetActive(false);
                PlayerPart[7].SetActive(false);

                //Player.transform.position = HidePlayer.transform.position;
                Driver.SetActive(true);

                Player.GetComponent<Rigidbody>().isKinematic = true;
                playerControl.GetComponent<PlayerController>().enabled = false;
                Player.GetComponent<ThirdPersonCharacter>().enabled = false;
                carExitBtn.GetComponent<Image>().enabled = true;

                Player.transform.parent = Car.transform;

                carCam.SetActive(true);
                PlayerCam.SetActive(false);
                carCam.transform.parent = null;
                PlayerCam.transform.parent = carCam.transform;
                PlayerControl[0].GetComponent<Image>().enabled = false;
                PlayerControl[1].GetComponent<Image>().enabled = false;
                PlayerControl[2].GetComponent<Image>().enabled = false;
                PlayerControl[3].GetComponent<Image>().enabled = false;
                PlayerControl[4].GetComponent<Image>().enabled = false;
                PlayerControl[5].GetComponent<Image>().enabled = false;
                CarControl[0].GetComponent<Image>().enabled = true;
                CarControl[1].GetComponent<Image>().enabled = true;
                CarControl[2].GetComponent<Image>().enabled = true;
                CarControl[3].GetComponent<Image>().enabled = true;
                CarControl[4].GetComponent<Image>().enabled = true;
            }
        }
    }

    public void exitCar()
    {
        Player.GetComponent<Rigidbody>().isKinematic = false;
        playerControl.GetComponent<PlayerController>().enabled = true;
        Player.GetComponent<ThirdPersonCharacter>().enabled = true;
        carExitBtn.GetComponent<Image>().enabled = false;
        Player.transform.rotation = Quaternion.Euler(0, 0, 0);
        PlayerPart[0].SetActive(true);
        PlayerPart[1].SetActive(true);
        PlayerPart[2].SetActive(true);
        PlayerPart[3].SetActive(true);
        PlayerPart[4].SetActive(true);
        PlayerPart[5].SetActive(true);
        PlayerPart[6].SetActive(true);
        PlayerPart[7].SetActive(true);

        Player.transform.parent = playerControl.transform;

        carCam.SetActive(false);
        PlayerCam.SetActive(true);
        PlayerCam.transform.parent = null;
        carCam.transform.parent = PlayerCam.transform;

        Player.GetComponent<Rigidbody>().AddForce(Player.transform.up * 7, ForceMode.Impulse);
        Player.GetComponent<Rigidbody>().AddForce(-Car.transform.right * 3, ForceMode.Impulse);

        //Player.transform.position = carExit.transform.position;
        Driver.SetActive(false);
        PlayerControl[0].GetComponent<Image>().enabled = true;
        PlayerControl[1].GetComponent<Image>().enabled = true;
        PlayerControl[2].GetComponent<Image>().enabled = true;
        PlayerControl[3].GetComponent<Image>().enabled = true;
        PlayerControl[4].GetComponent<Image>().enabled = true;
        PlayerControl[5].GetComponent<Image>().enabled = true;
        CarControl[0].GetComponent<Image>().enabled = false;
        CarControl[1].GetComponent<Image>().enabled = false;
        CarControl[2].GetComponent<Image>().enabled = false;
        CarControl[3].GetComponent<Image>().enabled = false;
        CarControl[4].GetComponent<Image>().enabled = false;
    }

	public void Fall(){
		hips.GetComponent <PlayerFixCoords> ().enabled = false;
		Head.GetComponent <ConstantForce> ().enabled = false;
		rightHand.GetComponent <ConstantForce> ().enabled = false;
		leftHand.GetComponent <ConstantForce> ().enabled = false;
		Col.GetComponent <CapsuleCollider> ().enabled = false;
		Player.GetComponent <Animator> ().enabled = false;
        Player.GetComponent<Rigidbody>().isKinematic = true;
		gameObject.GetComponent <CapsuleCollider> ().enabled = false;
		Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
		hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.None;
		hips.GetComponent <Rigidbody> ().AddForce (-1 * Player.transform.forward * 80, ForceMode.Impulse);
		hips.GetComponent <Rigidbody> ().AddForce (Player.transform.up * 80, ForceMode.Impulse);
		StartCoroutine (SlowMo ());
		StartCoroutine (TimeDelay ());
		if (Hp > 0)
			StartCoroutine (RaiseDelay ());
		else {
			StartCoroutine (loseDelay ());
			Controls.SetActive (false);
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed1.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy1.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy1Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed2.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy2.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy2Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed3.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy3.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy3Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed4.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy4.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy4Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed5.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy5.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy5Colforhit.GetComponent <EnemyFall> ().Throw();
		}
		if (playerControl.GetComponent <PlayerController> ().Grabbed6.activeInHierarchy == true) {
			playerControl.GetComponent <PlayerController> ().Enemy6.SetActive (true);
			playerControl.GetComponent <PlayerController> ().enemy6Colforhit.GetComponent <EnemyFall> ().Throw();
		}
	}

    public void AirAttack()
    {
        hips.GetComponent<PlayerFixCoords>().enabled = false;
        Head.GetComponent<ConstantForce>().enabled = false;
        rightHand.GetComponent<ConstantForce>().enabled = false;
        leftHand.GetComponent<ConstantForce>().enabled = false;
        //Col.GetComponent<CapsuleCollider>().enabled = false;
        Player.GetComponent<Animator>().enabled = false;
        Player.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        Head.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        hips.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        //hips.GetComponent<Rigidbody>().AddForce(Player.transform.forward * 80, ForceMode.Impulse);
        hips.GetComponent<Rigidbody>().AddForce(-Player.transform.up * 200, ForceMode.Impulse);
        hips.GetComponent<Rigidbody>().AddForce(Player.transform.forward * 100, ForceMode.Impulse);
        AirAttackCol.GetComponent<SphereCollider>().enabled = true;
        //StartCoroutine(SlowMo());
        //StartCoroutine(TimeDelay());
        StartCoroutine(RaiseDelay());
        StartCoroutine(AirColDelay());
    }


	public IEnumerator SlowMo(){
		yield return new WaitForSeconds (0.3f);
        Time.timeScale = 0.2f;
        //Time.fixedDeltaTime = Time.timeScale * 0.05f;
    }
	public IEnumerator TimeDelay(){
		yield return new WaitForSeconds (0.5f);
		Time.timeScale = 1;
        //Time.fixedDeltaTime = Time.unscaledDeltaTime;
	}

	public IEnumerator RaiseDelay(){
		yield return new WaitForSeconds (2f);
		fixCoords.transform.position = new Vector3 (hips.transform.position.x, hips.transform.position.y, hips.transform.position.z);
		fixCoords.SetActive (true);
		Head.GetComponent <ConstantForce> ().enabled = true;
		rightHand.GetComponent <ConstantForce> ().enabled = true;
		leftHand.GetComponent <ConstantForce> ().enabled = true;
		Col.GetComponent <CapsuleCollider> ().enabled = true;
		gameObject.GetComponent <CapsuleCollider> ().enabled = true;
		StartCoroutine (RaiseDelay1 ());
//		Head.transform.rotation = Quaternion.identity;
//		Head.transform.localRotation = Quaternion.Euler (0, 0, 0);
		Head.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.FreezeRotation;
		hips.GetComponent <Rigidbody> (). constraints = RigidbodyConstraints.FreezePosition;
		yield return new WaitForSeconds (0.2f);
		hips.GetComponent <PlayerFixCoords> ().enabled = true;
        Player.GetComponent<Rigidbody>().isKinematic = false;
    }
	public IEnumerator RaiseDelay1(){
		yield return new WaitForSeconds (0.5f);
		Player.GetComponent <Animator> ().enabled = true;
	}
	public IEnumerator loseDelay(){
		yield return new WaitForSeconds (3f);
		losePan.SetActive (true);
	}

    public IEnumerator AirColDelay()
    {
        yield return new WaitForSeconds(2f);
        AirAttackCol.GetComponent<SphereCollider>().enabled = false;
    }
}
