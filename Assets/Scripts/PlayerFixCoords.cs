﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class PlayerFixCoords : MonoBehaviour {

	public GameObject hips;
	public GameObject PlayerFix;
	public GameObject colForhit;
	public GameObject MenuSc;
	public GameObject losePan;
	public GameObject forHit;
    public GameObject Enemy;
	bool score = true;
    public float height = 0.447f;
    public GameObject PlayerCam;
//	public bool jumpInd;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		hips.transform.position = new Vector3 (PlayerFix.transform.position.x, PlayerFix.transform.position.y + height, PlayerFix.transform.position.z);
	}
		
//	private void OnTriggerExit (Collider col){
//		if (col.CompareTag ("Obstacle")) {
//			GetComponent <PlayerFixCoords> ().enabled = true;
//		}
//	}
//	public void Jump (){
//		if (jumpInd)
//			PlayerFix.GetComponent<Rigidbody> ().AddForce (new Vector3 (0,90,0) * 0.07f, ForceMode.Impulse);
//	}

	private void OnTriggerEnter (Collider col){
		if (col.gameObject.name == "FallTrigger"){
			Destroy (PlayerFix);
            Destroy(Enemy, 3);
            if (score)
				score = false;
			else
				score = true;
			if (score == false) {
				if (forHit.GetComponent <EnemyFall> ().Hp > 0)
					MenuSc.GetComponent <Menu> ().Score += 1;
			}
            PlayerCam.GetComponent<FreeLookCam>().enabled = false;
            losePan.SetActive (true);
		}
	}
//	private void OnTriggerExit (Collider col){
//		jumpInd = false;
//	}
}
