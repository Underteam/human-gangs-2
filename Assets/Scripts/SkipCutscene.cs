﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCutscene : MonoBehaviour
{

    public GameObject skipBtn;
    public GameObject Menu;
    public GameObject Loading;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SkipSpawn()
    {
        skipBtn.SetActive(true);
        StartCoroutine(skipHide());
    }
    public IEnumerator skipHide()
    {
        yield return new WaitForSeconds(2f);
        skipBtn.SetActive(false);
    }
    
    public void Skip()
    {
        Menu.GetComponent<Menu>().ShowAd();
        Loading.SetActive(true);
        StartCoroutine(SkipDelay());
    }
    public IEnumerator SkipDelay()
    {
        yield return new WaitForSeconds(0.1f);
        Application.LoadLevel(2);
    }
}
