﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = 0.04f;
        //Time.fixedDeltaTime = Time.timeScale * 0.01f;
    }

    private void OnEnable()
    {
        Time.timeScale = 0.04f;
    }
}
