﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public string ObjCol;
    public GameObject Sound;
    GameObject sound;
    public int Time;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == ObjCol)
        {
            sound = Instantiate(Sound, transform.position, transform.rotation);
            Destroy(sound, Time);
        }
    }
}
