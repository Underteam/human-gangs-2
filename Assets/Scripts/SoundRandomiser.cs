﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundRandomiser : MonoBehaviour
{
    public AudioSource _as;
    public AudioClip[] audioArray;


    void Awake()
    {
        _as.GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _as.clip = audioArray[Random.Range(0, audioArray.Length)];
        _as.PlayOneShot(_as.clip);
        GetComponent<AudioSource>().pitch = Random.Range(1, 1.3f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
