﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGlass90 : MonoBehaviour {

	public GameObject crashedGlass;
	public GameObject fixSize;
	GameObject crashedGlass1;
	GameObject fixSize1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter (Collider col){
		if (col.CompareTag ("PlayerFists")) {
			Instantiate (fixSize, transform.position, Quaternion.identity);
			crashedGlass1 = Instantiate (crashedGlass, transform.position, Quaternion.Euler (gameObject.transform.rotation.x - 90, gameObject.transform.rotation.y + 90, gameObject.transform.rotation.z));
//			crashedGlass1.transform.localRotation = Quaternion.Euler (gameObject.transform.localRotation.x - 90, gameObject.transform.localRotation.y, gameObject.transform.localRotation.z);
			crashedGlass1.transform.localScale = new Vector3 (gameObject.transform.localScale.x * 0.09f, gameObject.transform.localScale.z * 0.09f, gameObject.transform.localScale.y * 0.09f);
			Destroy (gameObject);
//			crashedGlass1.transform.parent = fixSize1.transform;
		}
	}
}
