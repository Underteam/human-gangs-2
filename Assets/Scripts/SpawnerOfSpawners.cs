﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerOfSpawners : MonoBehaviour
{

    public GameObject enemySpawner;
    public GameObject enemySpawner1;
    public GameObject enemySpawner2;
    public GameObject enemySpawner3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Throwing"))
        {
            if (col.gameObject.name != "canister")
            {
                StartCoroutine(Spawn());
            }
        }
    }
    public IEnumerator Spawn()
    {
        yield return new WaitForSeconds(0.5f);
        enemySpawner.SetActive(true);
        enemySpawner1.SetActive(true);
        enemySpawner2.SetActive(true);
        enemySpawner3.SetActive(true);
    }
}
