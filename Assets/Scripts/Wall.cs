﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    public GameObject Debris;
    GameObject debris;
    public GameObject CarCam;
    public GameObject SlowCam;
    public GameObject wall;
    public GameObject Car;
    public GameObject CarEnter;
    public GameObject Player;
    public GameObject Smoke;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "ColliderBottom")
        {
            Destroy(wall);
            gameObject.GetComponent<BoxCollider>().enabled = false;
            debris = Instantiate(Debris, transform.position, Debris.transform.rotation);
            Car.GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderForStop").transform.forward * 40000, ForceMode.Impulse);
            Car.GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderForStop").transform.up * 10000, ForceMode.Impulse);
            Smoke.SetActive(true);
            CarEnter.SetActive(false);
            CarCam.SetActive(false);
            SlowCam.SetActive(true);
            Time.timeScale = 0.2f;
            //Time.fixedDeltaTime = Time.timeScale * 0.05f;
            StartCoroutine(NormalDelay());
        }
    }
    public IEnumerator NormalDelay()
    {
        yield return new WaitForSeconds(0.75f);
        CarCam.SetActive(true);
        SlowCam.SetActive(false);
        Time.timeScale = 1;
        //Time.fixedDeltaTime = Time.unscaledDeltaTime;
        Player.GetComponent<PlayerFall>().exitCar();
        Smoke.SetActive(true);
    }
}
