﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall1 : MonoBehaviour
{

    public GameObject Debris;
    GameObject debris;
    public GameObject wall;
    public GameObject Spawner1;
    public GameObject Spawner2;
    public GameObject Spawner3;
    public GameObject Spawner4;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "WreckBall")
        {
            Destroy(wall);
            gameObject.GetComponent<BoxCollider>().enabled = false;
            debris = Instantiate(Debris, transform.position, Debris.transform.rotation);
            Spawner1.SetActive(true);
            Spawner2.SetActive(true);
            Spawner3.SetActive(true);
            Spawner4.SetActive(true);
        }
    }
}
