﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class WaterLose : MonoBehaviour
{

    public GameObject losePan;
    public GameObject Controls;
    public GameObject PlayerCam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "FallTrigger")
        {
            losePan.SetActive(true);
            Controls.SetActive(false);
            PlayerCam.GetComponent<AutoCam>().enabled = false;
        }
    }
}
