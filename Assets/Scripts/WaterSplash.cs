﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSplash : MonoBehaviour
{

    public GameObject Splash;
    GameObject splash;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Water")
        {
            splash = Instantiate(Splash, transform.position, Quaternion.Euler(90, 0, 0));
            Destroy(splash, 2f);
        }
    }
}
