﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WreckBall : MonoBehaviour
{

    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Rigidbody>().velocity.magnitude >= 3)
        {
            transform.gameObject.tag = "Throwing";
        }
        else
        {
            transform.gameObject.tag = "Untagged";
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("PlayerFists"))
        {
            GetComponent<Rigidbody>().AddForce(Player.transform.forward * 100, ForceMode.Impulse);
            //transform.gameObject.tag = "Throwing";
            //StartCoroutine(tagDelay());
        }
    }
    public IEnumerator tagDelay()
    {
        yield return new WaitForSeconds(8f);
        transform.gameObject.tag = "Untagged";
    }
}
