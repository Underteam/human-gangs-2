﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zabor : MonoBehaviour
{

    public GameObject Debris;
    GameObject debris;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "ColliderBottom")
        {
            debris = Instantiate(Debris, transform.position, Debris.transform.rotation);
            debris.transform.localScale = gameObject.transform.localScale;
            Destroy(gameObject);
        }
    }
}
