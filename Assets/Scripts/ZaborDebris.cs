﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZaborDebris : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, UnityEngine.Random.Range(4f, 6f));
        GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderForStop").transform.forward * 4, ForceMode.Impulse);
        //GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderBottom").transform.forward * 5, ForceMode.Impulse);
        //GetComponent<Rigidbody>().AddForce(GameObject.Find("ColliderBottom").transform.up * 5, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
