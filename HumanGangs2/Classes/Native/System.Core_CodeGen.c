﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000012 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000014 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000015 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000019 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000020 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000021 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000022 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000023 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000024 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000025 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000026 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000027 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000029 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002B System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000002C System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000002D System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000002E System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000002F System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000030 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000031 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000032 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000033 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000034 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000037 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000038 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003B System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003C System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003D System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000003E System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000003F System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000040 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000041 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000042 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000043 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000044 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000045 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[69] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[69] = 
{
	0,
	19,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x02000004, { 22, 4 } },
	{ 0x02000005, { 26, 9 } },
	{ 0x02000006, { 35, 7 } },
	{ 0x02000007, { 42, 10 } },
	{ 0x02000008, { 52, 1 } },
	{ 0x02000009, { 53, 34 } },
	{ 0x0200000B, { 87, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[89] = 
{
	{ (Il2CppRGCTXDataType)2, 13853 },
	{ (Il2CppRGCTXDataType)3, 7420 },
	{ (Il2CppRGCTXDataType)2, 13854 },
	{ (Il2CppRGCTXDataType)2, 13855 },
	{ (Il2CppRGCTXDataType)3, 7421 },
	{ (Il2CppRGCTXDataType)2, 13856 },
	{ (Il2CppRGCTXDataType)2, 13857 },
	{ (Il2CppRGCTXDataType)3, 7422 },
	{ (Il2CppRGCTXDataType)2, 13858 },
	{ (Il2CppRGCTXDataType)3, 7423 },
	{ (Il2CppRGCTXDataType)2, 13859 },
	{ (Il2CppRGCTXDataType)3, 7424 },
	{ (Il2CppRGCTXDataType)3, 7425 },
	{ (Il2CppRGCTXDataType)2, 9948 },
	{ (Il2CppRGCTXDataType)3, 7426 },
	{ (Il2CppRGCTXDataType)2, 9950 },
	{ (Il2CppRGCTXDataType)2, 13860 },
	{ (Il2CppRGCTXDataType)3, 7427 },
	{ (Il2CppRGCTXDataType)2, 9953 },
	{ (Il2CppRGCTXDataType)2, 9955 },
	{ (Il2CppRGCTXDataType)2, 13861 },
	{ (Il2CppRGCTXDataType)3, 7428 },
	{ (Il2CppRGCTXDataType)3, 7429 },
	{ (Il2CppRGCTXDataType)3, 7430 },
	{ (Il2CppRGCTXDataType)2, 9960 },
	{ (Il2CppRGCTXDataType)3, 7431 },
	{ (Il2CppRGCTXDataType)3, 7432 },
	{ (Il2CppRGCTXDataType)2, 9969 },
	{ (Il2CppRGCTXDataType)2, 13862 },
	{ (Il2CppRGCTXDataType)3, 7433 },
	{ (Il2CppRGCTXDataType)3, 7434 },
	{ (Il2CppRGCTXDataType)2, 9971 },
	{ (Il2CppRGCTXDataType)2, 13776 },
	{ (Il2CppRGCTXDataType)3, 7435 },
	{ (Il2CppRGCTXDataType)3, 7436 },
	{ (Il2CppRGCTXDataType)3, 7437 },
	{ (Il2CppRGCTXDataType)2, 9978 },
	{ (Il2CppRGCTXDataType)2, 13863 },
	{ (Il2CppRGCTXDataType)3, 7438 },
	{ (Il2CppRGCTXDataType)3, 7439 },
	{ (Il2CppRGCTXDataType)3, 7064 },
	{ (Il2CppRGCTXDataType)3, 7440 },
	{ (Il2CppRGCTXDataType)3, 7441 },
	{ (Il2CppRGCTXDataType)2, 9987 },
	{ (Il2CppRGCTXDataType)2, 13864 },
	{ (Il2CppRGCTXDataType)3, 7442 },
	{ (Il2CppRGCTXDataType)3, 7443 },
	{ (Il2CppRGCTXDataType)3, 7444 },
	{ (Il2CppRGCTXDataType)3, 7445 },
	{ (Il2CppRGCTXDataType)3, 7446 },
	{ (Il2CppRGCTXDataType)3, 7070 },
	{ (Il2CppRGCTXDataType)3, 7447 },
	{ (Il2CppRGCTXDataType)3, 7448 },
	{ (Il2CppRGCTXDataType)3, 7449 },
	{ (Il2CppRGCTXDataType)2, 13865 },
	{ (Il2CppRGCTXDataType)3, 7450 },
	{ (Il2CppRGCTXDataType)3, 7451 },
	{ (Il2CppRGCTXDataType)2, 10004 },
	{ (Il2CppRGCTXDataType)3, 7452 },
	{ (Il2CppRGCTXDataType)2, 10004 },
	{ (Il2CppRGCTXDataType)3, 7453 },
	{ (Il2CppRGCTXDataType)2, 10021 },
	{ (Il2CppRGCTXDataType)3, 7454 },
	{ (Il2CppRGCTXDataType)3, 7455 },
	{ (Il2CppRGCTXDataType)3, 7456 },
	{ (Il2CppRGCTXDataType)2, 13866 },
	{ (Il2CppRGCTXDataType)3, 7457 },
	{ (Il2CppRGCTXDataType)3, 7458 },
	{ (Il2CppRGCTXDataType)3, 7459 },
	{ (Il2CppRGCTXDataType)2, 10001 },
	{ (Il2CppRGCTXDataType)3, 7460 },
	{ (Il2CppRGCTXDataType)3, 7461 },
	{ (Il2CppRGCTXDataType)2, 10006 },
	{ (Il2CppRGCTXDataType)3, 7462 },
	{ (Il2CppRGCTXDataType)1, 13867 },
	{ (Il2CppRGCTXDataType)2, 10005 },
	{ (Il2CppRGCTXDataType)3, 7463 },
	{ (Il2CppRGCTXDataType)1, 10005 },
	{ (Il2CppRGCTXDataType)1, 10001 },
	{ (Il2CppRGCTXDataType)2, 13866 },
	{ (Il2CppRGCTXDataType)2, 10005 },
	{ (Il2CppRGCTXDataType)2, 10003 },
	{ (Il2CppRGCTXDataType)2, 10007 },
	{ (Il2CppRGCTXDataType)3, 7464 },
	{ (Il2CppRGCTXDataType)3, 7465 },
	{ (Il2CppRGCTXDataType)3, 7466 },
	{ (Il2CppRGCTXDataType)2, 10002 },
	{ (Il2CppRGCTXDataType)3, 7467 },
	{ (Il2CppRGCTXDataType)2, 10017 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	69,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	89,
	s_rgctxValues,
	NULL,
};
