extern "C" void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_SharedInternals();
	RegisterModule_SharedInternals();

	void RegisterModule_Core();
	RegisterModule_Core();

	void RegisterModule_AI();
	RegisterModule_AI();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_Terrain();
	RegisterModule_Terrain();

	void RegisterModule_TerrainPhysics();
	RegisterModule_TerrainPhysics();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_Umbra();
	RegisterModule_Umbra();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_Vehicles();
	RegisterModule_Vehicles();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_ImageConversion();
	RegisterModule_ImageConversion();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

	void RegisterModule_UnityAnalytics();
	RegisterModule_UnityAnalytics();

	void RegisterModule_GameCenter();
	RegisterModule_GameCenter();

	void RegisterModule_TLS();
	RegisterModule_TLS();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

	void RegisterModule_CrashReporting();
	RegisterModule_CrashReporting();

}

template <typename T> void RegisterUnityClass(const char*);
template <typename T> void RegisterStrippedType(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

namespace ObjectProduceTestTypes { class Derived; } 
namespace ObjectProduceTestTypes { class SubDerived; } 
class EditorExtension; template <> void RegisterUnityClass<EditorExtension>(const char*);
namespace Unity { class Component; } template <> void RegisterUnityClass<Unity::Component>(const char*);
class Behaviour; template <> void RegisterUnityClass<Behaviour>(const char*);
class Animation; template <> void RegisterUnityClass<Animation>(const char*);
class Animator; template <> void RegisterUnityClass<Animator>(const char*);
class AudioBehaviour; template <> void RegisterUnityClass<AudioBehaviour>(const char*);
class AudioListener; template <> void RegisterUnityClass<AudioListener>(const char*);
class AudioSource; template <> void RegisterUnityClass<AudioSource>(const char*);
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterUnityClass<Camera>(const char*);
namespace UI { class Canvas; } template <> void RegisterUnityClass<UI::Canvas>(const char*);
namespace UI { class CanvasGroup; } template <> void RegisterUnityClass<UI::CanvasGroup>(const char*);
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterUnityClass<Collider2D>(const char*);
class BoxCollider2D; 
class CapsuleCollider2D; 
class CircleCollider2D; 
class CompositeCollider2D; 
class EdgeCollider2D; 
class PolygonCollider2D; 
class TilemapCollider2D; 
class ConstantForce; template <> void RegisterUnityClass<ConstantForce>(const char*);
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; template <> void RegisterUnityClass<FlareLayer>(const char*);
class GUIElement; template <> void RegisterUnityClass<GUIElement>(const char*);
namespace TextRenderingPrivate { class GUIText; } template <> void RegisterUnityClass<TextRenderingPrivate::GUIText>(const char*);
class GUITexture; template <> void RegisterUnityClass<GUITexture>(const char*);
class GUILayer; template <> void RegisterUnityClass<GUILayer>(const char*);
class GridLayout; 
class Grid; 
class Tilemap; 
class Halo; 
class HaloLayer; 
class IConstraint; 
class AimConstraint; 
class LookAtConstraint; 
class ParentConstraint; 
class PositionConstraint; 
class RotationConstraint; 
class ScaleConstraint; 
class Joint2D; 
class AnchoredJoint2D; 
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; 
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; 
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterUnityClass<Light>(const char*);
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterUnityClass<MonoBehaviour>(const char*);
class NavMeshAgent; template <> void RegisterUnityClass<NavMeshAgent>(const char*);
class NavMeshObstacle; 
class OffMeshLink; 
class ParticleSystemForceField; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class PlayableDirector; 
class Projector; 
class ReflectionProbe; template <> void RegisterUnityClass<ReflectionProbe>(const char*);
class Skybox; template <> void RegisterUnityClass<Skybox>(const char*);
class SortingGroup; 
class StreamingController; 
class Terrain; template <> void RegisterUnityClass<Terrain>(const char*);
class VideoPlayer; 
class VisualEffect; 
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterUnityClass<UI::CanvasRenderer>(const char*);
class Collider; template <> void RegisterUnityClass<Collider>(const char*);
class BoxCollider; template <> void RegisterUnityClass<BoxCollider>(const char*);
class CapsuleCollider; template <> void RegisterUnityClass<CapsuleCollider>(const char*);
class CharacterController; template <> void RegisterUnityClass<CharacterController>(const char*);
class MeshCollider; template <> void RegisterUnityClass<MeshCollider>(const char*);
class SphereCollider; template <> void RegisterUnityClass<SphereCollider>(const char*);
class TerrainCollider; template <> void RegisterUnityClass<TerrainCollider>(const char*);
class WheelCollider; template <> void RegisterUnityClass<WheelCollider>(const char*);
class FakeComponent; 
namespace Unity { class Joint; } template <> void RegisterUnityClass<Unity::Joint>(const char*);
namespace Unity { class CharacterJoint; } template <> void RegisterUnityClass<Unity::CharacterJoint>(const char*);
namespace Unity { class ConfigurableJoint; } template <> void RegisterUnityClass<Unity::ConfigurableJoint>(const char*);
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } 
namespace Unity { class SpringJoint; } template <> void RegisterUnityClass<Unity::SpringJoint>(const char*);
class LODGroup; template <> void RegisterUnityClass<LODGroup>(const char*);
class MeshFilter; template <> void RegisterUnityClass<MeshFilter>(const char*);
class OcclusionArea; template <> void RegisterUnityClass<OcclusionArea>(const char*);
class OcclusionPortal; 
class ParticleSystem; template <> void RegisterUnityClass<ParticleSystem>(const char*);
class Renderer; template <> void RegisterUnityClass<Renderer>(const char*);
class BillboardRenderer; 
class LineRenderer; 
class RendererFake; 
class MeshRenderer; template <> void RegisterUnityClass<MeshRenderer>(const char*);
class ParticleSystemRenderer; template <> void RegisterUnityClass<ParticleSystemRenderer>(const char*);
class SkinnedMeshRenderer; template <> void RegisterUnityClass<SkinnedMeshRenderer>(const char*);
class SpriteMask; 
class SpriteRenderer; template <> void RegisterUnityClass<SpriteRenderer>(const char*);
class SpriteShapeRenderer; 
class TilemapRenderer; 
class TrailRenderer; template <> void RegisterUnityClass<TrailRenderer>(const char*);
class VFXRenderer; 
class Rigidbody; template <> void RegisterUnityClass<Rigidbody>(const char*);
class Rigidbody2D; template <> void RegisterUnityClass<Rigidbody2D>(const char*);
namespace TextRenderingPrivate { class TextMesh; } 
class Transform; template <> void RegisterUnityClass<Transform>(const char*);
namespace UI { class RectTransform; } template <> void RegisterUnityClass<UI::RectTransform>(const char*);
class Tree; 
class WorldAnchor; 
class GameObject; template <> void RegisterUnityClass<GameObject>(const char*);
class NamedObject; template <> void RegisterUnityClass<NamedObject>(const char*);
class AssetBundle; 
class AssetBundleManifest; 
class ScriptedImporter; 
class AudioMixer; 
class AudioMixerController; 
class AudioMixerGroup; 
class AudioMixerGroupController; 
class AudioMixerSnapshot; 
class AudioMixerSnapshotController; 
class Avatar; template <> void RegisterUnityClass<Avatar>(const char*);
class AvatarMask; 
class BillboardAsset; 
class ComputeShader; template <> void RegisterUnityClass<ComputeShader>(const char*);
class Flare; 
namespace TextRendering { class Font; } template <> void RegisterUnityClass<TextRendering::Font>(const char*);
class GameObjectRecorder; 
class LightProbes; template <> void RegisterUnityClass<LightProbes>(const char*);
class LocalizationAsset; 
class Material; template <> void RegisterUnityClass<Material>(const char*);
class ProceduralMaterial; 
class Mesh; template <> void RegisterUnityClass<Mesh>(const char*);
class Motion; template <> void RegisterUnityClass<Motion>(const char*);
class AnimationClip; template <> void RegisterUnityClass<AnimationClip>(const char*);
class PreviewAnimationClip; 
class NavMeshData; template <> void RegisterUnityClass<NavMeshData>(const char*);
class OcclusionCullingData; template <> void RegisterUnityClass<OcclusionCullingData>(const char*);
class PhysicMaterial; template <> void RegisterUnityClass<PhysicMaterial>(const char*);
class PhysicsMaterial2D; 
class PreloadData; template <> void RegisterUnityClass<PreloadData>(const char*);
class RuntimeAnimatorController; template <> void RegisterUnityClass<RuntimeAnimatorController>(const char*);
class AnimatorController; template <> void RegisterUnityClass<AnimatorController>(const char*);
class AnimatorOverrideController; template <> void RegisterUnityClass<AnimatorOverrideController>(const char*);
class SampleClip; template <> void RegisterUnityClass<SampleClip>(const char*);
class AudioClip; template <> void RegisterUnityClass<AudioClip>(const char*);
class Shader; template <> void RegisterUnityClass<Shader>(const char*);
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterUnityClass<Sprite>(const char*);
class SpriteAtlas; template <> void RegisterUnityClass<SpriteAtlas>(const char*);
class SubstanceArchive; 
class TerrainData; template <> void RegisterUnityClass<TerrainData>(const char*);
class TerrainLayer; template <> void RegisterUnityClass<TerrainLayer>(const char*);
class TextAsset; template <> void RegisterUnityClass<TextAsset>(const char*);
class CGProgram; 
class MonoScript; template <> void RegisterUnityClass<MonoScript>(const char*);
class Texture; template <> void RegisterUnityClass<Texture>(const char*);
class BaseVideoTexture; 
class MovieTexture; 
class WebCamTexture; 
class CubemapArray; template <> void RegisterUnityClass<CubemapArray>(const char*);
class LowerResBlitTexture; template <> void RegisterUnityClass<LowerResBlitTexture>(const char*);
class ProceduralTexture; 
class RenderTexture; template <> void RegisterUnityClass<RenderTexture>(const char*);
class CustomRenderTexture; 
class SparseTexture; 
class Texture2D; template <> void RegisterUnityClass<Texture2D>(const char*);
class Cubemap; template <> void RegisterUnityClass<Cubemap>(const char*);
class Texture2DArray; template <> void RegisterUnityClass<Texture2DArray>(const char*);
class Texture3D; template <> void RegisterUnityClass<Texture3D>(const char*);
class VideoClip; 
class VisualEffectAsset; 
class VisualEffectResource; 
class GameManager; template <> void RegisterUnityClass<GameManager>(const char*);
class GlobalGameManager; template <> void RegisterUnityClass<GlobalGameManager>(const char*);
class AudioManager; template <> void RegisterUnityClass<AudioManager>(const char*);
class BuildSettings; template <> void RegisterUnityClass<BuildSettings>(const char*);
class DelayedCallManager; template <> void RegisterUnityClass<DelayedCallManager>(const char*);
class GraphicsSettings; template <> void RegisterUnityClass<GraphicsSettings>(const char*);
class InputManager; template <> void RegisterUnityClass<InputManager>(const char*);
class MonoManager; template <> void RegisterUnityClass<MonoManager>(const char*);
class NavMeshProjectSettings; template <> void RegisterUnityClass<NavMeshProjectSettings>(const char*);
class Physics2DSettings; template <> void RegisterUnityClass<Physics2DSettings>(const char*);
class PhysicsManager; template <> void RegisterUnityClass<PhysicsManager>(const char*);
class PlayerSettings; template <> void RegisterUnityClass<PlayerSettings>(const char*);
class QualitySettings; template <> void RegisterUnityClass<QualitySettings>(const char*);
class ResourceManager; template <> void RegisterUnityClass<ResourceManager>(const char*);
class RuntimeInitializeOnLoadManager; template <> void RegisterUnityClass<RuntimeInitializeOnLoadManager>(const char*);
class ScriptMapper; template <> void RegisterUnityClass<ScriptMapper>(const char*);
class StreamingManager; 
class TagManager; template <> void RegisterUnityClass<TagManager>(const char*);
class TimeManager; template <> void RegisterUnityClass<TimeManager>(const char*);
class UnityConnectSettings; template <> void RegisterUnityClass<UnityConnectSettings>(const char*);
class VFXManager; 
class LevelGameManager; template <> void RegisterUnityClass<LevelGameManager>(const char*);
class LightmapSettings; template <> void RegisterUnityClass<LightmapSettings>(const char*);
class NavMeshSettings; template <> void RegisterUnityClass<NavMeshSettings>(const char*);
class OcclusionCullingSettings; template <> void RegisterUnityClass<OcclusionCullingSettings>(const char*);
class RenderSettings; template <> void RegisterUnityClass<RenderSettings>(const char*);
class NativeObjectType; 
class PropertyModificationsTargetTestObject; 
class SerializableManagedHost; 
class SerializableManagedRefTestClass; 
namespace ObjectProduceTestTypes { class SiblingDerived; } 
class TestObjectVectorPairStringBool; 
class TestObjectWithSerializedAnimationCurve; 
class TestObjectWithSerializedArray; 
class TestObjectWithSerializedMapStringBool; 
class TestObjectWithSerializedMapStringNonAlignedStruct; 
class TestObjectWithSpecialLayoutOne; 
class TestObjectWithSpecialLayoutTwo; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 109 non stripped classes
	//0. Camera
	RegisterUnityClass<Camera>("Core");
	//1. Behaviour
	RegisterUnityClass<Behaviour>("Core");
	//2. Unity::Component
	RegisterUnityClass<Unity::Component>("Core");
	//3. EditorExtension
	RegisterUnityClass<EditorExtension>("Core");
	//4. ReflectionProbe
	RegisterUnityClass<ReflectionProbe>("Core");
	//5. QualitySettings
	RegisterUnityClass<QualitySettings>("Core");
	//6. GlobalGameManager
	RegisterUnityClass<GlobalGameManager>("Core");
	//7. GameManager
	RegisterUnityClass<GameManager>("Core");
	//8. Renderer
	RegisterUnityClass<Renderer>("Core");
	//9. RenderSettings
	RegisterUnityClass<RenderSettings>("Core");
	//10. LevelGameManager
	RegisterUnityClass<LevelGameManager>("Core");
	//11. Shader
	RegisterUnityClass<Shader>("Core");
	//12. NamedObject
	RegisterUnityClass<NamedObject>("Core");
	//13. Material
	RegisterUnityClass<Material>("Core");
	//14. Light
	RegisterUnityClass<Light>("Core");
	//15. Skybox
	RegisterUnityClass<Skybox>("Core");
	//16. MeshFilter
	RegisterUnityClass<MeshFilter>("Core");
	//17. MeshRenderer
	RegisterUnityClass<MeshRenderer>("Core");
	//18. Mesh
	RegisterUnityClass<Mesh>("Core");
	//19. Texture
	RegisterUnityClass<Texture>("Core");
	//20. Texture2D
	RegisterUnityClass<Texture2D>("Core");
	//21. Cubemap
	RegisterUnityClass<Cubemap>("Core");
	//22. Texture3D
	RegisterUnityClass<Texture3D>("Core");
	//23. Texture2DArray
	RegisterUnityClass<Texture2DArray>("Core");
	//24. CubemapArray
	RegisterUnityClass<CubemapArray>("Core");
	//25. RenderTexture
	RegisterUnityClass<RenderTexture>("Core");
	//26. GUIElement
	RegisterUnityClass<GUIElement>("Core");
	//27. GUILayer
	RegisterUnityClass<GUILayer>("Core");
	//28. GameObject
	RegisterUnityClass<GameObject>("Core");
	//29. MonoBehaviour
	RegisterUnityClass<MonoBehaviour>("Core");
	//30. TextAsset
	RegisterUnityClass<TextAsset>("Core");
	//31. ComputeShader
	RegisterUnityClass<ComputeShader>("Core");
	//32. LowerResBlitTexture
	RegisterUnityClass<LowerResBlitTexture>("Core");
	//33. PreloadData
	RegisterUnityClass<PreloadData>("Core");
	//34. UI::RectTransform
	RegisterUnityClass<UI::RectTransform>("Core");
	//35. Transform
	RegisterUnityClass<Transform>("Core");
	//36. Sprite
	RegisterUnityClass<Sprite>("Core");
	//37. SpriteAtlas
	RegisterUnityClass<SpriteAtlas>("Core");
	//38. UI::Canvas
	RegisterUnityClass<UI::Canvas>("UI");
	//39. UI::CanvasGroup
	RegisterUnityClass<UI::CanvasGroup>("UI");
	//40. UI::CanvasRenderer
	RegisterUnityClass<UI::CanvasRenderer>("UI");
	//41. TextRenderingPrivate::GUIText
	RegisterUnityClass<TextRenderingPrivate::GUIText>("TextRendering");
	//42. TextRendering::Font
	RegisterUnityClass<TextRendering::Font>("TextRendering");
	//43. ParticleSystem
	RegisterUnityClass<ParticleSystem>("ParticleSystem");
	//44. ParticleSystemRenderer
	RegisterUnityClass<ParticleSystemRenderer>("ParticleSystem");
	//45. Animator
	RegisterUnityClass<Animator>("Animation");
	//46. AnimatorOverrideController
	RegisterUnityClass<AnimatorOverrideController>("Animation");
	//47. RuntimeAnimatorController
	RegisterUnityClass<RuntimeAnimatorController>("Animation");
	//48. Animation
	RegisterUnityClass<Animation>("Animation");
	//49. NavMeshAgent
	RegisterUnityClass<NavMeshAgent>("AI");
	//50. AudioClip
	RegisterUnityClass<AudioClip>("Audio");
	//51. SampleClip
	RegisterUnityClass<SampleClip>("Audio");
	//52. AudioListener
	RegisterUnityClass<AudioListener>("Audio");
	//53. AudioBehaviour
	RegisterUnityClass<AudioBehaviour>("Audio");
	//54. AudioSource
	RegisterUnityClass<AudioSource>("Audio");
	//55. PhysicMaterial
	RegisterUnityClass<PhysicMaterial>("Physics");
	//56. Rigidbody
	RegisterUnityClass<Rigidbody>("Physics");
	//57. Collider
	RegisterUnityClass<Collider>("Physics");
	//58. CharacterController
	RegisterUnityClass<CharacterController>("Physics");
	//59. CapsuleCollider
	RegisterUnityClass<CapsuleCollider>("Physics");
	//60. SphereCollider
	RegisterUnityClass<SphereCollider>("Physics");
	//61. ConstantForce
	RegisterUnityClass<ConstantForce>("Physics");
	//62. Unity::Joint
	RegisterUnityClass<Unity::Joint>("Physics");
	//63. Unity::SpringJoint
	RegisterUnityClass<Unity::SpringJoint>("Physics");
	//64. Rigidbody2D
	RegisterUnityClass<Rigidbody2D>("Physics2D");
	//65. Collider2D
	RegisterUnityClass<Collider2D>("Physics2D");
	//66. Terrain
	RegisterUnityClass<Terrain>("Terrain");
	//67. TerrainData
	RegisterUnityClass<TerrainData>("Terrain");
	//68. WheelCollider
	RegisterUnityClass<WheelCollider>("Vehicles");
	//69. GUITexture
	RegisterUnityClass<GUITexture>("Core");
	//70. FlareLayer
	RegisterUnityClass<FlareLayer>("Core");
	//71. TrailRenderer
	RegisterUnityClass<TrailRenderer>("Core");
	//72. MeshCollider
	RegisterUnityClass<MeshCollider>("Physics");
	//73. BoxCollider
	RegisterUnityClass<BoxCollider>("Physics");
	//74. MonoScript
	RegisterUnityClass<MonoScript>("Core");
	//75. UnityConnectSettings
	RegisterUnityClass<UnityConnectSettings>("UnityConnect");
	//76. NavMeshProjectSettings
	RegisterUnityClass<NavMeshProjectSettings>("AI");
	//77. AudioManager
	RegisterUnityClass<AudioManager>("Audio");
	//78. PhysicsManager
	RegisterUnityClass<PhysicsManager>("Physics");
	//79. PlayerSettings
	RegisterUnityClass<PlayerSettings>("Core");
	//80. TimeManager
	RegisterUnityClass<TimeManager>("Core");
	//81. InputManager
	RegisterUnityClass<InputManager>("Core");
	//82. TagManager
	RegisterUnityClass<TagManager>("Core");
	//83. MonoManager
	RegisterUnityClass<MonoManager>("Core");
	//84. GraphicsSettings
	RegisterUnityClass<GraphicsSettings>("Core");
	//85. DelayedCallManager
	RegisterUnityClass<DelayedCallManager>("Core");
	//86. BuildSettings
	RegisterUnityClass<BuildSettings>("Core");
	//87. RuntimeInitializeOnLoadManager
	RegisterUnityClass<RuntimeInitializeOnLoadManager>("Core");
	//88. ResourceManager
	RegisterUnityClass<ResourceManager>("Core");
	//89. ScriptMapper
	RegisterUnityClass<ScriptMapper>("Core");
	//90. Physics2DSettings
	RegisterUnityClass<Physics2DSettings>("Physics2D");
	//91. NavMeshData
	RegisterUnityClass<NavMeshData>("AI");
	//92. TerrainCollider
	RegisterUnityClass<TerrainCollider>("TerrainPhysics");
	//93. LightmapSettings
	RegisterUnityClass<LightmapSettings>("Core");
	//94. NavMeshSettings
	RegisterUnityClass<NavMeshSettings>("AI");
	//95. TerrainLayer
	RegisterUnityClass<TerrainLayer>("Terrain");
	//96. SkinnedMeshRenderer
	RegisterUnityClass<SkinnedMeshRenderer>("Core");
	//97. SpriteRenderer
	RegisterUnityClass<SpriteRenderer>("Core");
	//98. AnimationClip
	RegisterUnityClass<AnimationClip>("Animation");
	//99. Motion
	RegisterUnityClass<Motion>("Animation");
	//100. Unity::ConfigurableJoint
	RegisterUnityClass<Unity::ConfigurableJoint>("Physics");
	//101. AnimatorController
	RegisterUnityClass<AnimatorController>("Animation");
	//102. Avatar
	RegisterUnityClass<Avatar>("Animation");
	//103. LODGroup
	RegisterUnityClass<LODGroup>("Core");
	//104. OcclusionArea
	RegisterUnityClass<OcclusionArea>("Core");
	//105. LightProbes
	RegisterUnityClass<LightProbes>("Core");
	//106. Unity::CharacterJoint
	RegisterUnityClass<Unity::CharacterJoint>("Physics");
	//107. OcclusionCullingSettings
	RegisterUnityClass<OcclusionCullingSettings>("Umbra");
	//108. OcclusionCullingData
	RegisterUnityClass<OcclusionCullingData>("Umbra");

}
