﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Advertisements.IUnityEngineApplication UnityEngine.Advertisements.Advertisement::get_application()
extern void Advertisement_get_application_mC757B0832C73C6FA6494B12EDFF3B95D63BB66F7 ();
// 0x00000002 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_mF662AF5A614B7DD7A8F3424081DF0CF80F340B73 ();
// 0x00000003 System.Void UnityEngine.Advertisements.Advertisement::set_isInitialized(System.Boolean)
extern void Advertisement_set_isInitialized_m65C2DC7DB2CA71B271A3EA435DC3699D080A234A ();
// 0x00000004 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_m5B0CED8BEB7A6C2B8A1FC048D4E792DEE0F54771 ();
// 0x00000005 System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_m5FAB4C48BF22BE9BC452FE5AB830B520DED41B6D ();
// 0x00000006 System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m7206BD87D17C342D14584D187E2D8323D0E674AE ();
// 0x00000007 System.Void UnityEngine.Advertisements.Advertisement::set_isShowing(System.Boolean)
extern void Advertisement_set_isShowing_mECA72E12980B0E936EC37453A3EB1BF15112FB69 ();
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_m4F02032B24952524A7F78E9FFD305351A8AC031C ();
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_mCE35DED6BCC2D1BEB622AC1EC8B0D9C5012CE693 ();
// 0x0000000A System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_m496AE6FBC7B638EC6656114C7214A8461E3A381C ();
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_m4ADDA8C401AAA7CEB4F67547473168FD627D4D7D ();
// 0x0000000C System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_mE84D63248273F97E6FD60EF7BC78918DAB7B6084 ();
// 0x0000000D System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_m46712D70FDDD4A20B9AEDE2F8C20B01DB5E5CF70 ();
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement_<>c::.cctor()
extern void U3CU3Ec__cctor_mE05AFB8EB859F5D4B3A7AD85BB91B8661BCDA5CF ();
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement_<>c::.ctor()
extern void U3CU3Ec__ctor_m4E8177C1CDAC0C0D9D139F01E560E047A71EB68C ();
// 0x00000010 System.Void UnityEngine.Advertisements.Advertisement_<>c::<Initialize>b__26_0(System.Object,UnityEngine.Advertisements.StartEventArgs)
extern void U3CU3Ec_U3CInitializeU3Eb__26_0_m39FB67173812C2AAD12C7DD29FE91E41AFE5047E ();
// 0x00000011 System.Void UnityEngine.Advertisements.Advertisement_<>c::<Initialize>b__26_1(System.Object,UnityEngine.Advertisements.FinishEventArgs)
extern void U3CU3Ec_U3CInitializeU3Eb__26_1_mDA734CD31C827ADE72E40F3F621506534CB17A1A ();
// 0x00000012 System.Void UnityEngine.Advertisements.Advertisement_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m6BE4474FD09C50393B0508D712B6D071E463DDFE ();
// 0x00000013 System.Void UnityEngine.Advertisements.Advertisement_<>c__DisplayClass34_1::.ctor()
extern void U3CU3Ec__DisplayClass34_1__ctor_mA507DDA711B28DBFDC7EC93E6112E6928B6B06D6 ();
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement_<>c__DisplayClass34_1::<Show>b__0(System.Object,UnityEngine.Advertisements.FinishEventArgs)
extern void U3CU3Ec__DisplayClass34_1_U3CShowU3Eb__0_mEA109A9A592E8C3A874B79610D82DBB3282BA872 ();
// 0x00000015 System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerShowCallback(UnityEngine.Advertisements.Banner_unityAdsBannerShow)
extern void Banner_UnityAdsSetBannerShowCallback_mDE6872309C8CC14A1FA738C4778F408126641CE5 ();
// 0x00000016 System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerHideCallback(UnityEngine.Advertisements.Banner_unityAdsBannerHide)
extern void Banner_UnityAdsSetBannerHideCallback_m97CB62D5771AAFFAAF79F9104C9F8B8275E922CB ();
// 0x00000017 System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerClickCallback(UnityEngine.Advertisements.Banner_unityAdsBannerClick)
extern void Banner_UnityAdsSetBannerClickCallback_mF531575B976AC50332424F03860B2CD3B0BA8E1F ();
// 0x00000018 System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerErrorCallback(UnityEngine.Advertisements.Banner_unityAdsBannerError)
extern void Banner_UnityAdsSetBannerErrorCallback_m1196556CB1DCCB2FDBD20087FEFBD459706FC0BE ();
// 0x00000019 System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerUnloadCallback(UnityEngine.Advertisements.Banner_unityAdsBannerUnload)
extern void Banner_UnityAdsSetBannerUnloadCallback_m2403DE9CC02CA2A58DD8A521F5EE15527A80C238 ();
// 0x0000001A System.Void UnityEngine.Advertisements.Banner::UnityAdsSetBannerLoadCallback(UnityEngine.Advertisements.Banner_unityAdsBannerLoad)
extern void Banner_UnityAdsSetBannerLoadCallback_m667223A6642966818F82E5CC756D5743514DDFF1 ();
// 0x0000001B System.Void UnityEngine.Advertisements.Banner::UnityBannerInitialize()
extern void Banner_UnityBannerInitialize_m426A5C50CED4CD9531B44CA7E07741FFB3D02CE4 ();
// 0x0000001C System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.CallbackExecutor)
extern void Banner__ctor_mFEAE68397D23AF89DC73C82EB97ADAC8BB0AEE87 ();
// 0x0000001D System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidShow(System.String)
extern void Banner_UnityAdsBannerDidShow_m1321FD96EC051339EEAAFB993078E8A34E0A04D5 ();
// 0x0000001E System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidHide(System.String)
extern void Banner_UnityAdsBannerDidHide_m7FA7B5867DB8C387426CD09E2458D0301A0306B8 ();
// 0x0000001F System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerClick(System.String)
extern void Banner_UnityAdsBannerClick_m490698EE4D45486CD6287B473A5CB8A009006B9B ();
// 0x00000020 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidError(System.String)
extern void Banner_UnityAdsBannerDidError_mEF1C6D5479976AD64FE2D0A206F4615400AD2631 ();
// 0x00000021 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidUnload(System.String)
extern void Banner_UnityAdsBannerDidUnload_mF50EE813FA62B34648BC0289FFAEE4BFDF07E3F4 ();
// 0x00000022 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidLoad(System.String)
extern void Banner_UnityAdsBannerDidLoad_m236B0EBA23AF85D7E9ED37E4DFFF1BD0912BBFC5 ();
// 0x00000023 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerShow::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerShow__ctor_m73BC2EA679AB86A200F0F67528DEBCB3FDD3FEFB ();
// 0x00000024 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerShow::Invoke(System.String)
extern void unityAdsBannerShow_Invoke_m35B9216AF802FDAAE0C470184FDE044A2278A5F5 ();
// 0x00000025 System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerShow::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerShow_BeginInvoke_m649D12F52C12987EB86A9E4FF61DDD0085E1AD76 ();
// 0x00000026 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerShow::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerShow_EndInvoke_m01AA325AF17B0618966D6C128A229474E71AFFC3 ();
// 0x00000027 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerHide::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerHide__ctor_mC3866F8BE8C3D2208B47E9316143A8DF56F8685F ();
// 0x00000028 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerHide::Invoke(System.String)
extern void unityAdsBannerHide_Invoke_m36474E3D8EA1B7547BC4E87EB5AA736ACAD7C0AE ();
// 0x00000029 System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerHide::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerHide_BeginInvoke_m459CDD0F0F70F763F7DE57AC9ADDAEEA6003BCD2 ();
// 0x0000002A System.Void UnityEngine.Advertisements.Banner_unityAdsBannerHide::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerHide_EndInvoke_mB7D05B3D1AEE66479603EA253DF52C7CED95D194 ();
// 0x0000002B System.Void UnityEngine.Advertisements.Banner_unityAdsBannerClick::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerClick__ctor_m73FAE6A661472CD3C3FA80F7B7F57CDC3BF2BF1D ();
// 0x0000002C System.Void UnityEngine.Advertisements.Banner_unityAdsBannerClick::Invoke(System.String)
extern void unityAdsBannerClick_Invoke_m2D940B5CAD7675097C6E5AE2C201FB9AA3BFA94A ();
// 0x0000002D System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerClick::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerClick_BeginInvoke_m6B01B1502EC5A10151819FFD61419809E7973890 ();
// 0x0000002E System.Void UnityEngine.Advertisements.Banner_unityAdsBannerClick::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerClick_EndInvoke_m64B8B5B91F64D8E764FC7FC20D181AA93A8D9047 ();
// 0x0000002F System.Void UnityEngine.Advertisements.Banner_unityAdsBannerUnload::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerUnload__ctor_m9F57D12F623AA95431D7B5081C0DFB957B7A4279 ();
// 0x00000030 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerUnload::Invoke(System.String)
extern void unityAdsBannerUnload_Invoke_mB90B8A50EF91EBD87421CB63D2AE75991DC49C3F ();
// 0x00000031 System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerUnload::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerUnload_BeginInvoke_m6F6F5E4427E2D312CB1D5C5E56EB0210C5E6A142 ();
// 0x00000032 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerUnload::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerUnload_EndInvoke_m232C80DADB6FD3389568211E3B19D22CA26447BB ();
// 0x00000033 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerLoad::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerLoad__ctor_mBF891537A9ABEA3F881306097B8FC58C6BB4F5E5 ();
// 0x00000034 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerLoad::Invoke(System.String)
extern void unityAdsBannerLoad_Invoke_mCFF3C279E52861D49530292F6EC7AFF85B41355C ();
// 0x00000035 System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerLoad::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerLoad_BeginInvoke_m768D4A0D16D05921B20AC980C70BCCE85475EEFF ();
// 0x00000036 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerLoad::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerLoad_EndInvoke_m50107AA1748A687E30166439BA2A6351596EF852 ();
// 0x00000037 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerError::.ctor(System.Object,System.IntPtr)
extern void unityAdsBannerError__ctor_mA2DF8D14D3959DBE77401350F3779C892C9D7054 ();
// 0x00000038 System.Void UnityEngine.Advertisements.Banner_unityAdsBannerError::Invoke(System.String)
extern void unityAdsBannerError_Invoke_m396925217E408BD21A48870230CFE3A453E29024 ();
// 0x00000039 System.IAsyncResult UnityEngine.Advertisements.Banner_unityAdsBannerError::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsBannerError_BeginInvoke_m1C01B55501476A4D5DDC4CF5A1B88AB6C37CE16D ();
// 0x0000003A System.Void UnityEngine.Advertisements.Banner_unityAdsBannerError::EndInvoke(System.IAsyncResult)
extern void unityAdsBannerError_EndInvoke_m6DD4B762E19DAED3CF2470FD1DE2581EBAA13B00 ();
// 0x0000003B System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m2D51F079F618E2B6CF8B7BE1D9841CB29AA72933 ();
// 0x0000003C System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass41_0::<UnityAdsBannerDidShow>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass41_0_U3CUnityAdsBannerDidShowU3Eb__0_m69D35BDC42EFDB7D36866FB2163B213F0170E384 ();
// 0x0000003D System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m1E5F9CCFF319C02C8C88B45A50BEAF9CEC54D090 ();
// 0x0000003E System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass42_0::<UnityAdsBannerDidHide>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass42_0_U3CUnityAdsBannerDidHideU3Eb__0_m2A20F763F1ADB4C9F9940A9CA2CDA8381C8A943E ();
// 0x0000003F System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m4B02835B70D6C5E39FC78118621D9627CB3D672F ();
// 0x00000040 System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass44_0::<UnityAdsBannerDidError>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass44_0_U3CUnityAdsBannerDidErrorU3Eb__0_mEFCEF5F6BC5C8A3D12A6C74BE800B31FAA3C09BA ();
// 0x00000041 System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_m6B78DAF9D8003E79BEA23CE900B01A6D17F489A9 ();
// 0x00000042 System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass45_0::<UnityAdsBannerDidUnload>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass45_0_U3CUnityAdsBannerDidUnloadU3Eb__0_mDEC6FD7201180BA2974232008FFF8CA93F905762 ();
// 0x00000043 System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_mAD0F799C31E643FE422D0F95E2F704C2397B953C ();
// 0x00000044 System.Void UnityEngine.Advertisements.Banner_<>c__DisplayClass46_0::<UnityAdsBannerDidLoad>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass46_0_U3CUnityAdsBannerDidLoadU3Eb__0_m9A408D17B22C244E46A08CC8C5895AFE599A1477 ();
// 0x00000045 System.Void UnityEngine.Advertisements.CallbackExecutor::Post(System.Action`1<UnityEngine.Advertisements.CallbackExecutor>)
extern void CallbackExecutor_Post_mFB8875D0A2CD67040C6BB0A6A3B709BD70A95212 ();
// 0x00000046 System.Void UnityEngine.Advertisements.CallbackExecutor::Update()
extern void CallbackExecutor_Update_mEFA85AA1205BD2381A576BDE545131AF64515EE7 ();
// 0x00000047 System.Void UnityEngine.Advertisements.CallbackExecutor::.ctor()
extern void CallbackExecutor__ctor_mD58550026FCD6A39A3FCE27C3406B072AE916454 ();
// 0x00000048 UnityEngine.Advertisements.IPlatform UnityEngine.Advertisements.Creator::CreatePlatform()
extern void Creator_CreatePlatform_mB75C6B24BE4F64D8363C1000FF414BC2633B6C3E ();
// 0x00000049 System.Void UnityEngine.Advertisements.ErrorEventArgs::set_error(System.Int64)
extern void ErrorEventArgs_set_error_mE58EE659B976245D84061119635E13A5473FDC79 ();
// 0x0000004A System.Void UnityEngine.Advertisements.ErrorEventArgs::set_message(System.String)
extern void ErrorEventArgs_set_message_mFCE0BA0096601E79E1B5685E45C48703B4873488 ();
// 0x0000004B System.Void UnityEngine.Advertisements.ErrorEventArgs::.ctor(System.String)
extern void ErrorEventArgs__ctor_mBBAEDE3CE6F37FD5ABF0E44DCC0B01D57AF7BED5 ();
// 0x0000004C System.Void UnityEngine.Advertisements.FinishEventArgs::set_placementId(System.String)
extern void FinishEventArgs_set_placementId_mAF5E074AE1A0669B8CFD95DB4B208482182168E6 ();
// 0x0000004D UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_mF34EBB56E06BE43636E6D8E718DA76C79E8FC099 ();
// 0x0000004E System.Void UnityEngine.Advertisements.FinishEventArgs::set_showResult(UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs_set_showResult_mCAE6BADF58190DBE8EB7AB97A4909C3B8F6360AF ();
// 0x0000004F System.Void UnityEngine.Advertisements.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_mF3E7CECCB895E80D1100DB031D013EA429D67FC3 ();
// 0x00000050 System.Void UnityEngine.Advertisements.HideEventArgs::set_placementId(System.String)
extern void HideEventArgs_set_placementId_mAE0ED54018ADA1303591B2AB48879A6EA8871FCE ();
// 0x00000051 System.Void UnityEngine.Advertisements.HideEventArgs::.ctor(System.String)
extern void HideEventArgs__ctor_m1EC1F4E40C4DD1F53153C109A63266C2F59E8B91 ();
// 0x00000052 System.Void UnityEngine.Advertisements.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x00000053 System.Void UnityEngine.Advertisements.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
// 0x00000054 System.Void UnityEngine.Advertisements.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
// 0x00000055 System.Void UnityEngine.Advertisements.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
// 0x00000056 System.Boolean UnityEngine.Advertisements.IPlatform::get_isSupported()
// 0x00000057 System.String UnityEngine.Advertisements.IPlatform::get_version()
// 0x00000058 System.Void UnityEngine.Advertisements.IPlatform::Initialize(System.String,System.Boolean)
// 0x00000059 System.Boolean UnityEngine.Advertisements.IPlatform::IsReady(System.String)
// 0x0000005A System.Void UnityEngine.Advertisements.IPlatform::Show(System.String)
// 0x0000005B System.Void UnityEngine.Advertisements.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x0000005C System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsReady(System.String)
// 0x0000005D System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidError(System.String)
// 0x0000005E System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidStart(System.String)
// 0x0000005F System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x00000060 System.Boolean UnityEngine.Advertisements.IUnityEngineApplication::get_isEditor()
// 0x00000061 UnityEngine.RuntimePlatform UnityEngine.Advertisements.IUnityEngineApplication::get_platform()
// 0x00000062 System.String UnityEngine.Advertisements.IUnityEngineApplication::get_unityVersion()
// 0x00000063 System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_m3D57A54BB706D15F87689E7146B2E469B9D38EB8 ();
// 0x00000064 System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_m61A2E5BCBA11889F19FFB46D54C5E6522A3A109F ();
// 0x00000065 System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_m132363E97950AA8E84489C10A2E8AB235E5FBFFD ();
// 0x00000066 System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_m2C0BECA659B9BF4AA6AED9D26DDC51B212495AC2 ();
// 0x00000067 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_m9676F51FA4D83C34122D5A3C09DE0370E8FED751 ();
// 0x00000068 System.Void UnityEngine.Advertisements.Placeholder::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Placeholder_add_OnFinish_m8AD8AC99C56E68B8ADD977667A568A91DE652722 ();
// 0x00000069 System.Void UnityEngine.Advertisements.Placeholder::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Placeholder_remove_OnFinish_mC509CD9D959947D565B8C0B16E23E10BCAB97548 ();
// 0x0000006A System.Void UnityEngine.Advertisements.Placeholder::Awake()
extern void Placeholder_Awake_mEFAEE4B359F7E595F122A4F82EC06CD5AF466ED0 ();
// 0x0000006B System.Void UnityEngine.Advertisements.Placeholder::Show(System.String,System.Boolean)
extern void Placeholder_Show_m87688EA0899AC1E9B477BA79775444B7A88955D1 ();
// 0x0000006C System.Void UnityEngine.Advertisements.Placeholder::OnGUI()
extern void Placeholder_OnGUI_m4F6065AC26B8C273865ECEDDB6D2F797E2434A60 ();
// 0x0000006D System.Void UnityEngine.Advertisements.Placeholder::OnApplicationQuit()
extern void Placeholder_OnApplicationQuit_m8457720F0F58A98A87884F23C5CC45598426983A ();
// 0x0000006E System.Void UnityEngine.Advertisements.Placeholder::ModalWindowFunction(System.Int32)
extern void Placeholder_ModalWindowFunction_m9CB7CE434FCF95FF4C839082DB5C819061861EDF ();
// 0x0000006F System.Void UnityEngine.Advertisements.Placeholder::.ctor()
extern void Placeholder__ctor_m715C094C9E7264F9BA34C020BD890789A117E145 ();
// 0x00000070 System.Void UnityEngine.Advertisements.Platform::UnityAdsInitialize(System.String,System.Boolean)
extern void Platform_UnityAdsInitialize_m12066F222A47A1D9BFA1E58E5E294A81D925DCEE ();
// 0x00000071 System.Void UnityEngine.Advertisements.Platform::UnityAdsShow(System.String)
extern void Platform_UnityAdsShow_m621E757232512D4A7A83E0CDF32E2A2FC8E185BD ();
// 0x00000072 System.Boolean UnityEngine.Advertisements.Platform::UnityAdsIsSupported()
extern void Platform_UnityAdsIsSupported_m4244875E186264F1ABE851605618B7279A7E4510 ();
// 0x00000073 System.Boolean UnityEngine.Advertisements.Platform::UnityAdsIsReady(System.String)
extern void Platform_UnityAdsIsReady_mA66739543F31A74ADC5152659BD68B16154D3721 ();
// 0x00000074 System.String UnityEngine.Advertisements.Platform::UnityAdsGetVersion()
extern void Platform_UnityAdsGetVersion_m6ADA8E361B2C7F32F1A401DE2F52DF14F3DE5A5D ();
// 0x00000075 System.Void UnityEngine.Advertisements.Platform::UnityAdsSetMetaData(System.String,System.String)
extern void Platform_UnityAdsSetMetaData_mC7EC97A323B379CB0BDAF21729B16AD2F989BB88 ();
// 0x00000076 System.Void UnityEngine.Advertisements.Platform::UnityAdsSetReadyCallback(UnityEngine.Advertisements.Platform_unityAdsReady)
extern void Platform_UnityAdsSetReadyCallback_m7C98121F2E645E6375EB468FB521107A662992CD ();
// 0x00000077 System.Void UnityEngine.Advertisements.Platform::UnityAdsSetDidErrorCallback(UnityEngine.Advertisements.Platform_unityAdsDidError)
extern void Platform_UnityAdsSetDidErrorCallback_m2852FD0A31C567A497ADDD5DE40E8D72267B880A ();
// 0x00000078 System.Void UnityEngine.Advertisements.Platform::UnityAdsSetDidStartCallback(UnityEngine.Advertisements.Platform_unityAdsDidStart)
extern void Platform_UnityAdsSetDidStartCallback_m86F85EBE48E50C33898ADF0B627CC402BB3DBDAC ();
// 0x00000079 System.Void UnityEngine.Advertisements.Platform::UnityAdsSetDidFinishCallback(UnityEngine.Advertisements.Platform_unityAdsDidFinish)
extern void Platform_UnityAdsSetDidFinishCallback_mC120606E47176D42A5630177E932B5EB0807B168 ();
// 0x0000007A System.Void UnityEngine.Advertisements.Platform::UnityAdsReady(System.String)
extern void Platform_UnityAdsReady_m6C30BA59D4E8F9EDB83CD333BC681E3AD286BA72 ();
// 0x0000007B System.Void UnityEngine.Advertisements.Platform::UnityAdsDidError(System.Int64,System.String)
extern void Platform_UnityAdsDidError_mA49EE9C4421B319F6F6AD1F7BD6F6903BB7BBEC5 ();
// 0x0000007C System.Void UnityEngine.Advertisements.Platform::UnityAdsDidStart(System.String)
extern void Platform_UnityAdsDidStart_mEC8C778F41BEEBFE5C0048E971278E38DE98B562 ();
// 0x0000007D System.Void UnityEngine.Advertisements.Platform::UnityAdsDidFinish(System.String,System.Int64)
extern void Platform_UnityAdsDidFinish_m483CAD53E1D1D4D731D55E7E32CE62CADF28E433 ();
// 0x0000007E System.Void UnityEngine.Advertisements.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Platform_add_OnStart_mA841F76AEEA386EF8BF55E399428A03CDAAE82A9 ();
// 0x0000007F System.Void UnityEngine.Advertisements.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void Platform_remove_OnStart_mE116F7B3DBB1381EF0992877421A7905057B232F ();
// 0x00000080 System.Void UnityEngine.Advertisements.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Platform_add_OnFinish_m07C9F991073331BA7621B62CC0E3873D38D5221B ();
// 0x00000081 System.Void UnityEngine.Advertisements.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void Platform_remove_OnFinish_m37637C9DFF925ACD872CA5C5374B5B5D0FE38FEC ();
// 0x00000082 System.Void UnityEngine.Advertisements.Platform::.ctor()
extern void Platform__ctor_m048EA9E8E4D6EF20428865536CC5CB5DAA86E36E ();
// 0x00000083 System.Boolean UnityEngine.Advertisements.Platform::get_isSupported()
extern void Platform_get_isSupported_m295875912759503008B6B3A625F897E7DD12B55C ();
// 0x00000084 System.String UnityEngine.Advertisements.Platform::get_version()
extern void Platform_get_version_m3921D44913CF309825963B85033076BEBE6278C4 ();
// 0x00000085 System.Void UnityEngine.Advertisements.Platform::Initialize(System.String,System.Boolean)
extern void Platform_Initialize_mE427027CBE5B710F2FF329AB5E987669AEC4A18F ();
// 0x00000086 System.Boolean UnityEngine.Advertisements.Platform::IsReady(System.String)
extern void Platform_IsReady_mA989BBD8EA35E210E43A3827850CCDDFB8035187 ();
// 0x00000087 System.Void UnityEngine.Advertisements.Platform::Show(System.String)
extern void Platform_Show_mC38F81BFE5CD73CE8FF26B4260F0450D45577744 ();
// 0x00000088 System.Void UnityEngine.Advertisements.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mE3E745BB37177AAA9C9B3487DAA1D60B67D3DEE4 ();
// 0x00000089 System.Void UnityEngine.Advertisements.Platform_unityAdsReady::.ctor(System.Object,System.IntPtr)
extern void unityAdsReady__ctor_mAA242827664A52E2F37890AC85F02AEEFE373374 ();
// 0x0000008A System.Void UnityEngine.Advertisements.Platform_unityAdsReady::Invoke(System.String)
extern void unityAdsReady_Invoke_mCAF88BEED5BCD8BCBA20E6C067D30E5832045A04 ();
// 0x0000008B System.IAsyncResult UnityEngine.Advertisements.Platform_unityAdsReady::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsReady_BeginInvoke_mFDFB5A202AB99BCF6C59229B56070624255D58A9 ();
// 0x0000008C System.Void UnityEngine.Advertisements.Platform_unityAdsReady::EndInvoke(System.IAsyncResult)
extern void unityAdsReady_EndInvoke_m13720856C9C18B4DF124DCAE83B3425D9E85A350 ();
// 0x0000008D System.Void UnityEngine.Advertisements.Platform_unityAdsDidError::.ctor(System.Object,System.IntPtr)
extern void unityAdsDidError__ctor_m3AA06162A07045CB6B4FD4305F113DEEE87DFB92 ();
// 0x0000008E System.Void UnityEngine.Advertisements.Platform_unityAdsDidError::Invoke(System.Int64,System.String)
extern void unityAdsDidError_Invoke_mC78032FF4CFC9304C554557ECA061876010AC904 ();
// 0x0000008F System.IAsyncResult UnityEngine.Advertisements.Platform_unityAdsDidError::BeginInvoke(System.Int64,System.String,System.AsyncCallback,System.Object)
extern void unityAdsDidError_BeginInvoke_m0117BDDEE28A67E85B47F67375BDDCE2F759A38B ();
// 0x00000090 System.Void UnityEngine.Advertisements.Platform_unityAdsDidError::EndInvoke(System.IAsyncResult)
extern void unityAdsDidError_EndInvoke_m79AAB5EF4ACB0207A400DC071EA5DF7B9BDD5E5A ();
// 0x00000091 System.Void UnityEngine.Advertisements.Platform_unityAdsDidStart::.ctor(System.Object,System.IntPtr)
extern void unityAdsDidStart__ctor_m509F7ED5AC46AE977F258936596CD382853CD802 ();
// 0x00000092 System.Void UnityEngine.Advertisements.Platform_unityAdsDidStart::Invoke(System.String)
extern void unityAdsDidStart_Invoke_m0A69BA50153A3C52BB43B4628163FDC5FF3727EA ();
// 0x00000093 System.IAsyncResult UnityEngine.Advertisements.Platform_unityAdsDidStart::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsDidStart_BeginInvoke_m5539C024CE22557C7A56BEF8862773B4DDD9102D ();
// 0x00000094 System.Void UnityEngine.Advertisements.Platform_unityAdsDidStart::EndInvoke(System.IAsyncResult)
extern void unityAdsDidStart_EndInvoke_m470932811A5972797DDFAF531F8DC6C7597DD257 ();
// 0x00000095 System.Void UnityEngine.Advertisements.Platform_unityAdsDidFinish::.ctor(System.Object,System.IntPtr)
extern void unityAdsDidFinish__ctor_m213831AB4974588465E248841DD85ED3CA7CAEA8 ();
// 0x00000096 System.Void UnityEngine.Advertisements.Platform_unityAdsDidFinish::Invoke(System.String,System.Int64)
extern void unityAdsDidFinish_Invoke_m120D46241DC3550FDF293B445FF0610FB10903D3 ();
// 0x00000097 System.IAsyncResult UnityEngine.Advertisements.Platform_unityAdsDidFinish::BeginInvoke(System.String,System.Int64,System.AsyncCallback,System.Object)
extern void unityAdsDidFinish_BeginInvoke_m0D8581A18261A865E9A449C562424D5B42DCA760 ();
// 0x00000098 System.Void UnityEngine.Advertisements.Platform_unityAdsDidFinish::EndInvoke(System.IAsyncResult)
extern void unityAdsDidFinish_EndInvoke_m4685D455E77939184C112EB42EAC0DE3C22ECDA5 ();
// 0x00000099 System.Void UnityEngine.Advertisements.Platform_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m54D7D16092F17D3707EA406D3EC569545FC2B2D2 ();
// 0x0000009A System.Void UnityEngine.Advertisements.Platform_<>c__DisplayClass22_0::<UnityAdsDidStart>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass22_0_U3CUnityAdsDidStartU3Eb__0_m2169D46EDF1D38ABD6E87E0D1CE39E715DC6CE97 ();
// 0x0000009B System.Void UnityEngine.Advertisements.Platform_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mC1AD51C7876C8C1E84A7094B9A8786CF73AEDC39 ();
// 0x0000009C System.Void UnityEngine.Advertisements.Platform_<>c__DisplayClass23_0::<UnityAdsDidFinish>b__0(UnityEngine.Advertisements.CallbackExecutor)
extern void U3CU3Ec__DisplayClass23_0_U3CUnityAdsDidFinishU3Eb__0_m08EF1971835770E63017C720791F2E98CE1E3017 ();
// 0x0000009D System.Boolean UnityEngine.Advertisements.Purchasing::Initialize(UnityEngine.Advertisements.IPurchasingEventSender)
extern void Purchasing_Initialize_m08D7E8B577420676513E5FBD9E06457DF150F9F5 ();
// 0x0000009E System.Boolean UnityEngine.Advertisements.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_m6E97B736A19FA1912075D156702D7ADC5CF6BF13 ();
// 0x0000009F System.String UnityEngine.Advertisements.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_mE2DD4CE471F7227C179EBEE8295C0C0AF6D39D89 ();
// 0x000000A0 System.String UnityEngine.Advertisements.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_mDBDAFCF3BC92408DAFED36C4EEF415BBF58CCE6E ();
// 0x000000A1 System.Void UnityEngine.Advertisements.Purchasing::.cctor()
extern void Purchasing__cctor_m5145AB6BA4BBD601A85CE21C8E6DFE07C72DA6DA ();
// 0x000000A2 UnityEngine.Advertisements.PurchasingPlatform UnityEngine.Advertisements.PurchasingPlatform::get_Instance()
extern void PurchasingPlatform_get_Instance_mFA082AF75D56E5B446AA3E74296224390199E33A ();
// 0x000000A3 System.Void UnityEngine.Advertisements.PurchasingPlatform::set_Instance(UnityEngine.Advertisements.PurchasingPlatform)
extern void PurchasingPlatform_set_Instance_mABC5F5A04C0BC61086A39D117958BDFF031B0238 ();
// 0x000000A4 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsPurchasingDispatchReturnEvent(System.Int64,System.String)
extern void PurchasingPlatform_UnityAdsPurchasingDispatchReturnEvent_mBC63E930C32E6E7879C99C3B325E95E8D5A0535B ();
// 0x000000A5 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsSetDidInitiatePurchasingCommandCallback(UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingDidInitiatePurchasingCommand)
extern void PurchasingPlatform_UnityAdsSetDidInitiatePurchasingCommandCallback_m410EA692978BF45BC49B62D46190DCB62F411899 ();
// 0x000000A6 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsSetGetProductCatalogCallback(UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetProductCatalog)
extern void PurchasingPlatform_UnityAdsSetGetProductCatalogCallback_m6FBB2FC5113A0D9AE31591E96DC0A81FB790F6D7 ();
// 0x000000A7 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsSetGetVersionCallback(UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetPurchasingVersion)
extern void PurchasingPlatform_UnityAdsSetGetVersionCallback_mAF53D4DD38906A95DEDCA7AECF73568EA818D4D0 ();
// 0x000000A8 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsSetInitializePurchasingCallback(UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingInitialize)
extern void PurchasingPlatform_UnityAdsSetInitializePurchasingCallback_m95896FE0279A5309640C3CCC045E874868477D4C ();
// 0x000000A9 System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsDidInitiatePurchasingCommand(System.String)
extern void PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_m17319169B88EB108D5D92A20028BABE194EF66E2 ();
// 0x000000AA System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsPurchasingGetProductCatalog()
extern void PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_m816FF240957F8DC09FD3F700E02664C2EC09FA60 ();
// 0x000000AB System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsPurchasingGetPurchasingVersion()
extern void PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_mB8E821FA244D36B77E62140A8EF568FCC81826EC ();
// 0x000000AC System.Void UnityEngine.Advertisements.PurchasingPlatform::UnityAdsPurchasingInitialize()
extern void PurchasingPlatform_UnityAdsPurchasingInitialize_m5A7B8C88EF7061A9FDB3A964128923A5F4E05AF1 ();
// 0x000000AD System.Void UnityEngine.Advertisements.PurchasingPlatform::Initialize()
extern void PurchasingPlatform_Initialize_mCA2B8A23943779F604E10EFAEB336559BBF0C124 ();
// 0x000000AE System.Void UnityEngine.Advertisements.PurchasingPlatform::.ctor()
extern void PurchasingPlatform__ctor_mF382A2DD110588027C96ACBE00F27893F872CD96 ();
// 0x000000AF System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingDidInitiatePurchasingCommand::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingDidInitiatePurchasingCommand__ctor_mA6962E1A65D76BC291ED5AE8FB46A1250976D55C ();
// 0x000000B0 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingDidInitiatePurchasingCommand::Invoke(System.String)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_Invoke_mB2F0A4B7D058E470E0E76BD28C6537E040D2A1F3 ();
// 0x000000B1 System.IAsyncResult UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingDidInitiatePurchasingCommand::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_BeginInvoke_mA8F4600322D292F93AE3CA0A2A9AC4E34B725994 ();
// 0x000000B2 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingDidInitiatePurchasingCommand::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_EndInvoke_m02BC45C55920E118DA6E0F10C1B80B6E0CCFBFCD ();
// 0x000000B3 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetProductCatalog::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingGetProductCatalog__ctor_mCC18737B6434FD0ECAAD7025444A8B22E28035D1 ();
// 0x000000B4 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetProductCatalog::Invoke()
extern void unityAdsPurchasingGetProductCatalog_Invoke_m045602A713A67BA68849D446899D428D77FB4AE4 ();
// 0x000000B5 System.IAsyncResult UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetProductCatalog::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingGetProductCatalog_BeginInvoke_mC8A88AFF4CE88815542AF83FFC65AE5C4B5ED2E0 ();
// 0x000000B6 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetProductCatalog::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingGetProductCatalog_EndInvoke_mA6155C52D5B9C791F264D04C65420409AA4CCDF1 ();
// 0x000000B7 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetPurchasingVersion::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingGetPurchasingVersion__ctor_mD2AD2A9C54946F34E456B4F71E455D8E701C8E1A ();
// 0x000000B8 System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetPurchasingVersion::Invoke()
extern void unityAdsPurchasingGetPurchasingVersion_Invoke_m502DA69A27A56E42D861FCDA63DFD864EDA3E7C3 ();
// 0x000000B9 System.IAsyncResult UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetPurchasingVersion::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingGetPurchasingVersion_BeginInvoke_mD2A55C18117B94EBF2AC4C7905F686B7D5542BFA ();
// 0x000000BA System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingGetPurchasingVersion::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingGetPurchasingVersion_EndInvoke_mEBF7838D1ACA68A0C507DF032514C6A0954A5F8A ();
// 0x000000BB System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingInitialize::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingInitialize__ctor_mCD2B5CBC88CDFEB2C140D123C8987932396C61BC ();
// 0x000000BC System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingInitialize::Invoke()
extern void unityAdsPurchasingInitialize_Invoke_mE3A05B01B44934E620DC5B83C6B26662D18F939A ();
// 0x000000BD System.IAsyncResult UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingInitialize::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingInitialize_BeginInvoke_m7972F78AD4357ABB6478C818A944FE24C075D5A4 ();
// 0x000000BE System.Void UnityEngine.Advertisements.PurchasingPlatform_unityAdsPurchasingInitialize::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingInitialize_EndInvoke_m4C0942B97F89BAC167008F08DA6A099A7D52B1E1 ();
// 0x000000BF System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_m5E247F4BE62D87CEDC9EEDF425FD17727D1D4BC1 ();
// 0x000000C0 System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_m05BA00AFA9E93EF144750ABE216188F2DE8D46A5 ();
// 0x000000C1 System.Void UnityEngine.Advertisements.StartEventArgs::set_placementId(System.String)
extern void StartEventArgs_set_placementId_m0FA944E48945F818F6738B71307608B88B1C031A ();
// 0x000000C2 System.Void UnityEngine.Advertisements.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_mEE699F697CD14CF4726DECA2CDB0F460878D8167 ();
// 0x000000C3 System.Boolean UnityEngine.Advertisements.UnityEngineApplication::get_isEditor()
extern void UnityEngineApplication_get_isEditor_m5C6F82C2124E12F2AC736E0730BAF5002E7CABAE ();
// 0x000000C4 UnityEngine.RuntimePlatform UnityEngine.Advertisements.UnityEngineApplication::get_platform()
extern void UnityEngineApplication_get_platform_m572FC345EC13CF209651331F0C461A021B116B78 ();
// 0x000000C5 System.String UnityEngine.Advertisements.UnityEngineApplication::get_unityVersion()
extern void UnityEngineApplication_get_unityVersion_m66F8FD656B078DBD02A6C8FD0677B1D5172ACA72 ();
// 0x000000C6 System.Void UnityEngine.Advertisements.UnityEngineApplication::.ctor()
extern void UnityEngineApplication__ctor_mB0DEC22D6163A632D758898F35EDF318F259DD23 ();
// 0x000000C7 System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void UnsupportedPlatform_add_OnStart_m8144F6343387C37343C9FCB8708367C9A8FDD185 ();
// 0x000000C8 System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>)
extern void UnsupportedPlatform_remove_OnStart_mBB28588A77013805836556679762217859CF0F8C ();
// 0x000000C9 System.Void UnityEngine.Advertisements.UnsupportedPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void UnsupportedPlatform_add_OnFinish_m01CC031A011094D846146BDC5668E4EECB71B0CE ();
// 0x000000CA System.Void UnityEngine.Advertisements.UnsupportedPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>)
extern void UnsupportedPlatform_remove_OnFinish_mBFAAB26FB23EB6F2270660864F4E9F46DAA35558 ();
// 0x000000CB System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::get_isSupported()
extern void UnsupportedPlatform_get_isSupported_mD47EF34F2ACC88F56E0500133376A3A464C4FC22 ();
// 0x000000CC System.String UnityEngine.Advertisements.UnsupportedPlatform::get_version()
extern void UnsupportedPlatform_get_version_mC0127E4498ED6FD01C63F99AE3AF8D2C82AB36BA ();
// 0x000000CD System.Void UnityEngine.Advertisements.UnsupportedPlatform::Initialize(System.String,System.Boolean)
extern void UnsupportedPlatform_Initialize_m1520580611589FDF1F669EC863E2D29F1F9B03A0 ();
// 0x000000CE System.Boolean UnityEngine.Advertisements.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mBC069096DF109311086A716789FB58E8806A0BBF ();
// 0x000000CF System.Void UnityEngine.Advertisements.UnsupportedPlatform::Show(System.String)
extern void UnsupportedPlatform_Show_m7C150BA237BAB37F1492D3CBE0F65058E3BF9C53 ();
// 0x000000D0 System.Void UnityEngine.Advertisements.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_m6DB84E90EC050CF7A78EE8211B75E466DE6CA4F2 ();
// 0x000000D1 System.Void UnityEngine.Advertisements.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m9358913C11638C45C7FFC34585D6AC704DF9070B ();
// 0x000000D2 System.Void UnityEngine.Advertisements.UnsupportedPlatform::.cctor()
extern void UnsupportedPlatform__cctor_mCFBE08EDE9560DF72035B0C69DEBFDC5FBCB9444 ();
// 0x000000D3 System.Void UnityEngine.Advertisements.UnsupportedPlatform_NullBanner::.ctor()
extern void NullBanner__ctor_m04F759F6424B09A5B6E9A53FDFCFBC9B345A54B5 ();
// 0x000000D4 System.String UnityEngine.Advertisements.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m879C0DD0958450E9A94132817C86FB9460D4E6EC ();
// 0x000000D5 System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_m75B3D5A568A211CFC693F99052B4F28510B2C2F7 ();
// 0x000000D6 System.String UnityEngine.Advertisements.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mA5F0BAD3457C3AD33BFA8EBF9838DE8CDBAC4AA4 ();
// 0x000000D7 System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m72DEDCF4BB0BBC4924CB73371DA7A8F8BC2AA543 ();
// 0x000000D8 System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m98B011E994A3F904EBEBC210B5283B38D02B7A8A ();
// 0x000000D9 System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m2767CA41320D293B3C4AD8557078ECEA916CEEAD ();
// 0x000000DA System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m3DB88175576F595E3F5DA3A855AA7B04DC1DA3EF ();
// 0x000000DB System.Void UnityEngine.Advertisements.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mDF9463357736261EC67B7D534DB69AC8C9A7039C ();
static Il2CppMethodPointer s_methodPointers[219] = 
{
	Advertisement_get_application_mC757B0832C73C6FA6494B12EDFF3B95D63BB66F7,
	Advertisement_get_isInitialized_mF662AF5A614B7DD7A8F3424081DF0CF80F340B73,
	Advertisement_set_isInitialized_m65C2DC7DB2CA71B271A3EA435DC3699D080A234A,
	Advertisement_get_isSupported_m5B0CED8BEB7A6C2B8A1FC048D4E792DEE0F54771,
	Advertisement_get_version_m5FAB4C48BF22BE9BC452FE5AB830B520DED41B6D,
	Advertisement_get_isShowing_m7206BD87D17C342D14584D187E2D8323D0E674AE,
	Advertisement_set_isShowing_mECA72E12980B0E936EC37453A3EB1BF15112FB69,
	Advertisement__cctor_m4F02032B24952524A7F78E9FFD305351A8AC031C,
	Advertisement_Initialize_mCE35DED6BCC2D1BEB622AC1EC8B0D9C5012CE693,
	Advertisement_IsReady_m496AE6FBC7B638EC6656114C7214A8461E3A381C,
	Advertisement_Show_m4ADDA8C401AAA7CEB4F67547473168FD627D4D7D,
	Advertisement_Show_mE84D63248273F97E6FD60EF7BC78918DAB7B6084,
	Advertisement_SetMetaData_m46712D70FDDD4A20B9AEDE2F8C20B01DB5E5CF70,
	U3CU3Ec__cctor_mE05AFB8EB859F5D4B3A7AD85BB91B8661BCDA5CF,
	U3CU3Ec__ctor_m4E8177C1CDAC0C0D9D139F01E560E047A71EB68C,
	U3CU3Ec_U3CInitializeU3Eb__26_0_m39FB67173812C2AAD12C7DD29FE91E41AFE5047E,
	U3CU3Ec_U3CInitializeU3Eb__26_1_mDA734CD31C827ADE72E40F3F621506534CB17A1A,
	U3CU3Ec__DisplayClass34_0__ctor_m6BE4474FD09C50393B0508D712B6D071E463DDFE,
	U3CU3Ec__DisplayClass34_1__ctor_mA507DDA711B28DBFDC7EC93E6112E6928B6B06D6,
	U3CU3Ec__DisplayClass34_1_U3CShowU3Eb__0_mEA109A9A592E8C3A874B79610D82DBB3282BA872,
	Banner_UnityAdsSetBannerShowCallback_mDE6872309C8CC14A1FA738C4778F408126641CE5,
	Banner_UnityAdsSetBannerHideCallback_m97CB62D5771AAFFAAF79F9104C9F8B8275E922CB,
	Banner_UnityAdsSetBannerClickCallback_mF531575B976AC50332424F03860B2CD3B0BA8E1F,
	Banner_UnityAdsSetBannerErrorCallback_m1196556CB1DCCB2FDBD20087FEFBD459706FC0BE,
	Banner_UnityAdsSetBannerUnloadCallback_m2403DE9CC02CA2A58DD8A521F5EE15527A80C238,
	Banner_UnityAdsSetBannerLoadCallback_m667223A6642966818F82E5CC756D5743514DDFF1,
	Banner_UnityBannerInitialize_m426A5C50CED4CD9531B44CA7E07741FFB3D02CE4,
	Banner__ctor_mFEAE68397D23AF89DC73C82EB97ADAC8BB0AEE87,
	Banner_UnityAdsBannerDidShow_m1321FD96EC051339EEAAFB993078E8A34E0A04D5,
	Banner_UnityAdsBannerDidHide_m7FA7B5867DB8C387426CD09E2458D0301A0306B8,
	Banner_UnityAdsBannerClick_m490698EE4D45486CD6287B473A5CB8A009006B9B,
	Banner_UnityAdsBannerDidError_mEF1C6D5479976AD64FE2D0A206F4615400AD2631,
	Banner_UnityAdsBannerDidUnload_mF50EE813FA62B34648BC0289FFAEE4BFDF07E3F4,
	Banner_UnityAdsBannerDidLoad_m236B0EBA23AF85D7E9ED37E4DFFF1BD0912BBFC5,
	unityAdsBannerShow__ctor_m73BC2EA679AB86A200F0F67528DEBCB3FDD3FEFB,
	unityAdsBannerShow_Invoke_m35B9216AF802FDAAE0C470184FDE044A2278A5F5,
	unityAdsBannerShow_BeginInvoke_m649D12F52C12987EB86A9E4FF61DDD0085E1AD76,
	unityAdsBannerShow_EndInvoke_m01AA325AF17B0618966D6C128A229474E71AFFC3,
	unityAdsBannerHide__ctor_mC3866F8BE8C3D2208B47E9316143A8DF56F8685F,
	unityAdsBannerHide_Invoke_m36474E3D8EA1B7547BC4E87EB5AA736ACAD7C0AE,
	unityAdsBannerHide_BeginInvoke_m459CDD0F0F70F763F7DE57AC9ADDAEEA6003BCD2,
	unityAdsBannerHide_EndInvoke_mB7D05B3D1AEE66479603EA253DF52C7CED95D194,
	unityAdsBannerClick__ctor_m73FAE6A661472CD3C3FA80F7B7F57CDC3BF2BF1D,
	unityAdsBannerClick_Invoke_m2D940B5CAD7675097C6E5AE2C201FB9AA3BFA94A,
	unityAdsBannerClick_BeginInvoke_m6B01B1502EC5A10151819FFD61419809E7973890,
	unityAdsBannerClick_EndInvoke_m64B8B5B91F64D8E764FC7FC20D181AA93A8D9047,
	unityAdsBannerUnload__ctor_m9F57D12F623AA95431D7B5081C0DFB957B7A4279,
	unityAdsBannerUnload_Invoke_mB90B8A50EF91EBD87421CB63D2AE75991DC49C3F,
	unityAdsBannerUnload_BeginInvoke_m6F6F5E4427E2D312CB1D5C5E56EB0210C5E6A142,
	unityAdsBannerUnload_EndInvoke_m232C80DADB6FD3389568211E3B19D22CA26447BB,
	unityAdsBannerLoad__ctor_mBF891537A9ABEA3F881306097B8FC58C6BB4F5E5,
	unityAdsBannerLoad_Invoke_mCFF3C279E52861D49530292F6EC7AFF85B41355C,
	unityAdsBannerLoad_BeginInvoke_m768D4A0D16D05921B20AC980C70BCCE85475EEFF,
	unityAdsBannerLoad_EndInvoke_m50107AA1748A687E30166439BA2A6351596EF852,
	unityAdsBannerError__ctor_mA2DF8D14D3959DBE77401350F3779C892C9D7054,
	unityAdsBannerError_Invoke_m396925217E408BD21A48870230CFE3A453E29024,
	unityAdsBannerError_BeginInvoke_m1C01B55501476A4D5DDC4CF5A1B88AB6C37CE16D,
	unityAdsBannerError_EndInvoke_m6DD4B762E19DAED3CF2470FD1DE2581EBAA13B00,
	U3CU3Ec__DisplayClass41_0__ctor_m2D51F079F618E2B6CF8B7BE1D9841CB29AA72933,
	U3CU3Ec__DisplayClass41_0_U3CUnityAdsBannerDidShowU3Eb__0_m69D35BDC42EFDB7D36866FB2163B213F0170E384,
	U3CU3Ec__DisplayClass42_0__ctor_m1E5F9CCFF319C02C8C88B45A50BEAF9CEC54D090,
	U3CU3Ec__DisplayClass42_0_U3CUnityAdsBannerDidHideU3Eb__0_m2A20F763F1ADB4C9F9940A9CA2CDA8381C8A943E,
	U3CU3Ec__DisplayClass44_0__ctor_m4B02835B70D6C5E39FC78118621D9627CB3D672F,
	U3CU3Ec__DisplayClass44_0_U3CUnityAdsBannerDidErrorU3Eb__0_mEFCEF5F6BC5C8A3D12A6C74BE800B31FAA3C09BA,
	U3CU3Ec__DisplayClass45_0__ctor_m6B78DAF9D8003E79BEA23CE900B01A6D17F489A9,
	U3CU3Ec__DisplayClass45_0_U3CUnityAdsBannerDidUnloadU3Eb__0_mDEC6FD7201180BA2974232008FFF8CA93F905762,
	U3CU3Ec__DisplayClass46_0__ctor_mAD0F799C31E643FE422D0F95E2F704C2397B953C,
	U3CU3Ec__DisplayClass46_0_U3CUnityAdsBannerDidLoadU3Eb__0_m9A408D17B22C244E46A08CC8C5895AFE599A1477,
	CallbackExecutor_Post_mFB8875D0A2CD67040C6BB0A6A3B709BD70A95212,
	CallbackExecutor_Update_mEFA85AA1205BD2381A576BDE545131AF64515EE7,
	CallbackExecutor__ctor_mD58550026FCD6A39A3FCE27C3406B072AE916454,
	Creator_CreatePlatform_mB75C6B24BE4F64D8363C1000FF414BC2633B6C3E,
	ErrorEventArgs_set_error_mE58EE659B976245D84061119635E13A5473FDC79,
	ErrorEventArgs_set_message_mFCE0BA0096601E79E1B5685E45C48703B4873488,
	ErrorEventArgs__ctor_mBBAEDE3CE6F37FD5ABF0E44DCC0B01D57AF7BED5,
	FinishEventArgs_set_placementId_mAF5E074AE1A0669B8CFD95DB4B208482182168E6,
	FinishEventArgs_get_showResult_mF34EBB56E06BE43636E6D8E718DA76C79E8FC099,
	FinishEventArgs_set_showResult_mCAE6BADF58190DBE8EB7AB97A4909C3B8F6360AF,
	FinishEventArgs__ctor_mF3E7CECCB895E80D1100DB031D013EA429D67FC3,
	HideEventArgs_set_placementId_mAE0ED54018ADA1303591B2AB48879A6EA8871FCE,
	HideEventArgs__ctor_m1EC1F4E40C4DD1F53153C109A63266C2F59E8B91,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_m3D57A54BB706D15F87689E7146B2E469B9D38EB8,
	MetaData_set_category_m61A2E5BCBA11889F19FFB46D54C5E6522A3A109F,
	MetaData__ctor_m132363E97950AA8E84489C10A2E8AB235E5FBFFD,
	MetaData_Set_m2C0BECA659B9BF4AA6AED9D26DDC51B212495AC2,
	MetaData_ToJSON_m9676F51FA4D83C34122D5A3C09DE0370E8FED751,
	Placeholder_add_OnFinish_m8AD8AC99C56E68B8ADD977667A568A91DE652722,
	Placeholder_remove_OnFinish_mC509CD9D959947D565B8C0B16E23E10BCAB97548,
	Placeholder_Awake_mEFAEE4B359F7E595F122A4F82EC06CD5AF466ED0,
	Placeholder_Show_m87688EA0899AC1E9B477BA79775444B7A88955D1,
	Placeholder_OnGUI_m4F6065AC26B8C273865ECEDDB6D2F797E2434A60,
	Placeholder_OnApplicationQuit_m8457720F0F58A98A87884F23C5CC45598426983A,
	Placeholder_ModalWindowFunction_m9CB7CE434FCF95FF4C839082DB5C819061861EDF,
	Placeholder__ctor_m715C094C9E7264F9BA34C020BD890789A117E145,
	Platform_UnityAdsInitialize_m12066F222A47A1D9BFA1E58E5E294A81D925DCEE,
	Platform_UnityAdsShow_m621E757232512D4A7A83E0CDF32E2A2FC8E185BD,
	Platform_UnityAdsIsSupported_m4244875E186264F1ABE851605618B7279A7E4510,
	Platform_UnityAdsIsReady_mA66739543F31A74ADC5152659BD68B16154D3721,
	Platform_UnityAdsGetVersion_m6ADA8E361B2C7F32F1A401DE2F52DF14F3DE5A5D,
	Platform_UnityAdsSetMetaData_mC7EC97A323B379CB0BDAF21729B16AD2F989BB88,
	Platform_UnityAdsSetReadyCallback_m7C98121F2E645E6375EB468FB521107A662992CD,
	Platform_UnityAdsSetDidErrorCallback_m2852FD0A31C567A497ADDD5DE40E8D72267B880A,
	Platform_UnityAdsSetDidStartCallback_m86F85EBE48E50C33898ADF0B627CC402BB3DBDAC,
	Platform_UnityAdsSetDidFinishCallback_mC120606E47176D42A5630177E932B5EB0807B168,
	Platform_UnityAdsReady_m6C30BA59D4E8F9EDB83CD333BC681E3AD286BA72,
	Platform_UnityAdsDidError_mA49EE9C4421B319F6F6AD1F7BD6F6903BB7BBEC5,
	Platform_UnityAdsDidStart_mEC8C778F41BEEBFE5C0048E971278E38DE98B562,
	Platform_UnityAdsDidFinish_m483CAD53E1D1D4D731D55E7E32CE62CADF28E433,
	Platform_add_OnStart_mA841F76AEEA386EF8BF55E399428A03CDAAE82A9,
	Platform_remove_OnStart_mE116F7B3DBB1381EF0992877421A7905057B232F,
	Platform_add_OnFinish_m07C9F991073331BA7621B62CC0E3873D38D5221B,
	Platform_remove_OnFinish_m37637C9DFF925ACD872CA5C5374B5B5D0FE38FEC,
	Platform__ctor_m048EA9E8E4D6EF20428865536CC5CB5DAA86E36E,
	Platform_get_isSupported_m295875912759503008B6B3A625F897E7DD12B55C,
	Platform_get_version_m3921D44913CF309825963B85033076BEBE6278C4,
	Platform_Initialize_mE427027CBE5B710F2FF329AB5E987669AEC4A18F,
	Platform_IsReady_mA989BBD8EA35E210E43A3827850CCDDFB8035187,
	Platform_Show_mC38F81BFE5CD73CE8FF26B4260F0450D45577744,
	Platform_SetMetaData_mE3E745BB37177AAA9C9B3487DAA1D60B67D3DEE4,
	unityAdsReady__ctor_mAA242827664A52E2F37890AC85F02AEEFE373374,
	unityAdsReady_Invoke_mCAF88BEED5BCD8BCBA20E6C067D30E5832045A04,
	unityAdsReady_BeginInvoke_mFDFB5A202AB99BCF6C59229B56070624255D58A9,
	unityAdsReady_EndInvoke_m13720856C9C18B4DF124DCAE83B3425D9E85A350,
	unityAdsDidError__ctor_m3AA06162A07045CB6B4FD4305F113DEEE87DFB92,
	unityAdsDidError_Invoke_mC78032FF4CFC9304C554557ECA061876010AC904,
	unityAdsDidError_BeginInvoke_m0117BDDEE28A67E85B47F67375BDDCE2F759A38B,
	unityAdsDidError_EndInvoke_m79AAB5EF4ACB0207A400DC071EA5DF7B9BDD5E5A,
	unityAdsDidStart__ctor_m509F7ED5AC46AE977F258936596CD382853CD802,
	unityAdsDidStart_Invoke_m0A69BA50153A3C52BB43B4628163FDC5FF3727EA,
	unityAdsDidStart_BeginInvoke_m5539C024CE22557C7A56BEF8862773B4DDD9102D,
	unityAdsDidStart_EndInvoke_m470932811A5972797DDFAF531F8DC6C7597DD257,
	unityAdsDidFinish__ctor_m213831AB4974588465E248841DD85ED3CA7CAEA8,
	unityAdsDidFinish_Invoke_m120D46241DC3550FDF293B445FF0610FB10903D3,
	unityAdsDidFinish_BeginInvoke_m0D8581A18261A865E9A449C562424D5B42DCA760,
	unityAdsDidFinish_EndInvoke_m4685D455E77939184C112EB42EAC0DE3C22ECDA5,
	U3CU3Ec__DisplayClass22_0__ctor_m54D7D16092F17D3707EA406D3EC569545FC2B2D2,
	U3CU3Ec__DisplayClass22_0_U3CUnityAdsDidStartU3Eb__0_m2169D46EDF1D38ABD6E87E0D1CE39E715DC6CE97,
	U3CU3Ec__DisplayClass23_0__ctor_mC1AD51C7876C8C1E84A7094B9A8786CF73AEDC39,
	U3CU3Ec__DisplayClass23_0_U3CUnityAdsDidFinishU3Eb__0_m08EF1971835770E63017C720791F2E98CE1E3017,
	Purchasing_Initialize_m08D7E8B577420676513E5FBD9E06457DF150F9F5,
	Purchasing_InitiatePurchasingCommand_m6E97B736A19FA1912075D156702D7ADC5CF6BF13,
	Purchasing_GetPurchasingCatalog_mE2DD4CE471F7227C179EBEE8295C0C0AF6D39D89,
	Purchasing_GetPromoVersion_mDBDAFCF3BC92408DAFED36C4EEF415BBF58CCE6E,
	Purchasing__cctor_m5145AB6BA4BBD601A85CE21C8E6DFE07C72DA6DA,
	PurchasingPlatform_get_Instance_mFA082AF75D56E5B446AA3E74296224390199E33A,
	PurchasingPlatform_set_Instance_mABC5F5A04C0BC61086A39D117958BDFF031B0238,
	PurchasingPlatform_UnityAdsPurchasingDispatchReturnEvent_mBC63E930C32E6E7879C99C3B325E95E8D5A0535B,
	PurchasingPlatform_UnityAdsSetDidInitiatePurchasingCommandCallback_m410EA692978BF45BC49B62D46190DCB62F411899,
	PurchasingPlatform_UnityAdsSetGetProductCatalogCallback_m6FBB2FC5113A0D9AE31591E96DC0A81FB790F6D7,
	PurchasingPlatform_UnityAdsSetGetVersionCallback_mAF53D4DD38906A95DEDCA7AECF73568EA818D4D0,
	PurchasingPlatform_UnityAdsSetInitializePurchasingCallback_m95896FE0279A5309640C3CCC045E874868477D4C,
	PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_m17319169B88EB108D5D92A20028BABE194EF66E2,
	PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_m816FF240957F8DC09FD3F700E02664C2EC09FA60,
	PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_mB8E821FA244D36B77E62140A8EF568FCC81826EC,
	PurchasingPlatform_UnityAdsPurchasingInitialize_m5A7B8C88EF7061A9FDB3A964128923A5F4E05AF1,
	PurchasingPlatform_Initialize_mCA2B8A23943779F604E10EFAEB336559BBF0C124,
	PurchasingPlatform__ctor_mF382A2DD110588027C96ACBE00F27893F872CD96,
	unityAdsPurchasingDidInitiatePurchasingCommand__ctor_mA6962E1A65D76BC291ED5AE8FB46A1250976D55C,
	unityAdsPurchasingDidInitiatePurchasingCommand_Invoke_mB2F0A4B7D058E470E0E76BD28C6537E040D2A1F3,
	unityAdsPurchasingDidInitiatePurchasingCommand_BeginInvoke_mA8F4600322D292F93AE3CA0A2A9AC4E34B725994,
	unityAdsPurchasingDidInitiatePurchasingCommand_EndInvoke_m02BC45C55920E118DA6E0F10C1B80B6E0CCFBFCD,
	unityAdsPurchasingGetProductCatalog__ctor_mCC18737B6434FD0ECAAD7025444A8B22E28035D1,
	unityAdsPurchasingGetProductCatalog_Invoke_m045602A713A67BA68849D446899D428D77FB4AE4,
	unityAdsPurchasingGetProductCatalog_BeginInvoke_mC8A88AFF4CE88815542AF83FFC65AE5C4B5ED2E0,
	unityAdsPurchasingGetProductCatalog_EndInvoke_mA6155C52D5B9C791F264D04C65420409AA4CCDF1,
	unityAdsPurchasingGetPurchasingVersion__ctor_mD2AD2A9C54946F34E456B4F71E455D8E701C8E1A,
	unityAdsPurchasingGetPurchasingVersion_Invoke_m502DA69A27A56E42D861FCDA63DFD864EDA3E7C3,
	unityAdsPurchasingGetPurchasingVersion_BeginInvoke_mD2A55C18117B94EBF2AC4C7905F686B7D5542BFA,
	unityAdsPurchasingGetPurchasingVersion_EndInvoke_mEBF7838D1ACA68A0C507DF032514C6A0954A5F8A,
	unityAdsPurchasingInitialize__ctor_mCD2B5CBC88CDFEB2C140D123C8987932396C61BC,
	unityAdsPurchasingInitialize_Invoke_mE3A05B01B44934E620DC5B83C6B26662D18F939A,
	unityAdsPurchasingInitialize_BeginInvoke_m7972F78AD4357ABB6478C818A944FE24C075D5A4,
	unityAdsPurchasingInitialize_EndInvoke_m4C0942B97F89BAC167008F08DA6A099A7D52B1E1,
	ShowOptions_get_resultCallback_m5E247F4BE62D87CEDC9EEDF425FD17727D1D4BC1,
	ShowOptions_get_gamerSid_m05BA00AFA9E93EF144750ABE216188F2DE8D46A5,
	StartEventArgs_set_placementId_m0FA944E48945F818F6738B71307608B88B1C031A,
	StartEventArgs__ctor_mEE699F697CD14CF4726DECA2CDB0F460878D8167,
	UnityEngineApplication_get_isEditor_m5C6F82C2124E12F2AC736E0730BAF5002E7CABAE,
	UnityEngineApplication_get_platform_m572FC345EC13CF209651331F0C461A021B116B78,
	UnityEngineApplication_get_unityVersion_m66F8FD656B078DBD02A6C8FD0677B1D5172ACA72,
	UnityEngineApplication__ctor_mB0DEC22D6163A632D758898F35EDF318F259DD23,
	UnsupportedPlatform_add_OnStart_m8144F6343387C37343C9FCB8708367C9A8FDD185,
	UnsupportedPlatform_remove_OnStart_mBB28588A77013805836556679762217859CF0F8C,
	UnsupportedPlatform_add_OnFinish_m01CC031A011094D846146BDC5668E4EECB71B0CE,
	UnsupportedPlatform_remove_OnFinish_mBFAAB26FB23EB6F2270660864F4E9F46DAA35558,
	UnsupportedPlatform_get_isSupported_mD47EF34F2ACC88F56E0500133376A3A464C4FC22,
	UnsupportedPlatform_get_version_mC0127E4498ED6FD01C63F99AE3AF8D2C82AB36BA,
	UnsupportedPlatform_Initialize_m1520580611589FDF1F669EC863E2D29F1F9B03A0,
	UnsupportedPlatform_IsReady_mBC069096DF109311086A716789FB58E8806A0BBF,
	UnsupportedPlatform_Show_m7C150BA237BAB37F1492D3CBE0F65058E3BF9C53,
	UnsupportedPlatform_SetMetaData_m6DB84E90EC050CF7A78EE8211B75E466DE6CA4F2,
	UnsupportedPlatform__ctor_m9358913C11638C45C7FFC34585D6AC704DF9070B,
	UnsupportedPlatform__cctor_mCFBE08EDE9560DF72035B0C69DEBFDC5FBCB9444,
	NullBanner__ctor_m04F759F6424B09A5B6E9A53FDFCFBC9B345A54B5,
	Json_Serialize_m879C0DD0958450E9A94132817C86FB9460D4E6EC,
	Serializer__ctor_m75B3D5A568A211CFC693F99052B4F28510B2C2F7,
	Serializer_Serialize_mA5F0BAD3457C3AD33BFA8EBF9838DE8CDBAC4AA4,
	Serializer_SerializeValue_m72DEDCF4BB0BBC4924CB73371DA7A8F8BC2AA543,
	Serializer_SerializeObject_m98B011E994A3F904EBEBC210B5283B38D02B7A8A,
	Serializer_SerializeArray_m2767CA41320D293B3C4AD8557078ECEA916CEEAD,
	Serializer_SerializeString_m3DB88175576F595E3F5DA3A855AA7B04DC1DA3EF,
	Serializer_SerializeOther_mDF9463357736261EC67B7D534DB69AC8C9A7039C,
};
static const int32_t s_InvokerIndices[219] = 
{
	19,
	77,
	769,
	77,
	19,
	77,
	769,
	8,
	550,
	28,
	30,
	122,
	30,
	8,
	13,
	23,
	23,
	13,
	13,
	23,
	30,
	30,
	30,
	30,
	30,
	30,
	8,
	4,
	30,
	30,
	30,
	30,
	30,
	30,
	164,
	4,
	167,
	4,
	164,
	4,
	167,
	4,
	164,
	4,
	167,
	4,
	164,
	4,
	167,
	4,
	164,
	4,
	167,
	4,
	164,
	4,
	167,
	4,
	13,
	4,
	13,
	4,
	13,
	4,
	13,
	4,
	13,
	4,
	4,
	13,
	13,
	19,
	161,
	4,
	4,
	4,
	18,
	9,
	124,
	4,
	4,
	4,
	4,
	4,
	4,
	17,
	14,
	387,
	32,
	4,
	4,
	4,
	4,
	4,
	124,
	17,
	18,
	14,
	14,
	4,
	4,
	23,
	14,
	4,
	4,
	13,
	387,
	13,
	13,
	9,
	13,
	550,
	30,
	77,
	28,
	19,
	122,
	30,
	30,
	30,
	30,
	30,
	1561,
	30,
	27,
	4,
	4,
	4,
	4,
	13,
	17,
	14,
	387,
	32,
	4,
	4,
	164,
	4,
	167,
	4,
	164,
	1562,
	1563,
	4,
	164,
	4,
	167,
	4,
	164,
	129,
	1564,
	4,
	13,
	4,
	13,
	4,
	28,
	28,
	19,
	19,
	8,
	19,
	30,
	1561,
	30,
	30,
	30,
	30,
	30,
	8,
	8,
	8,
	13,
	13,
	164,
	4,
	167,
	4,
	164,
	13,
	16,
	4,
	164,
	13,
	16,
	4,
	164,
	13,
	16,
	4,
	14,
	14,
	4,
	4,
	17,
	18,
	14,
	13,
	4,
	4,
	4,
	4,
	17,
	14,
	387,
	32,
	4,
	4,
	13,
	8,
	13,
	0,
	13,
	0,
	4,
	4,
	4,
	4,
	4,
};
static const Il2CppTokenIndexPair s_reversePInvokeIndices[14] = 
{
	{ 0x0600001D, 0 },
	{ 0x0600001E, 1 },
	{ 0x0600001F, 2 },
	{ 0x06000020, 3 },
	{ 0x06000021, 4 },
	{ 0x06000022, 5 },
	{ 0x0600007A, 6 },
	{ 0x0600007B, 7 },
	{ 0x0600007C, 8 },
	{ 0x0600007D, 9 },
	{ 0x060000A9, 10 },
	{ 0x060000AA, 11 },
	{ 0x060000AB, 12 },
	{ 0x060000AC, 13 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AdvertisementsCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AdvertisementsCodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	219,
	s_methodPointers,
	s_InvokerIndices,
	14,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
